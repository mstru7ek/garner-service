package pl.garner.service.provider.audio

import org.junit.Test

import java.net.URI

import org.junit.Assert.assertTrue

class OxfordAudioUrlProviderTest {

    private val urlProvider = OxfordAudioUrlProvider()

    @Test
    fun should_receive_two_different_urls_for_single_word_query() {

        val resultList = urlProvider.fetchList("conch")

        assertTrue(resultList.size == 2)
        assertTrue(urlProvider.type == ProviderType.OXFORD)
        assertTrue(resultList.contains(URI.create("https://lex-audio.useremarkable.com/mp3/xconch_gb_1_8.mp3")))
        assertTrue(resultList.contains(URI.create("https://lex-audio.useremarkable.com/mp3/xconch_gb_1.mp3")))
    }

    @Test
    fun should_receive_two_different_urls_for_double_word_query() {

        val resultList = urlProvider.fetchList("vicious circle")

        assertTrue(resultList.size == 1)
        assertTrue(urlProvider.type == ProviderType.OXFORD)
        assertTrue(resultList.contains(URI.create("https://lex-audio.useremarkable.com/mp3/vicious_circle_1_gb_1.mp3")))
    }
}
