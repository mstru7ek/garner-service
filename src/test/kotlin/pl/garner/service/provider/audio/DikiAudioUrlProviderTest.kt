package pl.garner.service.provider.audio

import org.junit.Assert
import org.junit.Test

import java.net.URI

class DikiAudioUrlProviderTest {

    private val urlProvider = DikiAudioUrlProvider()

    @Test
    fun should_receive_two_different_urls_for_single_word_query() {

        val resultList = urlProvider.fetchList("invert")

        Assert.assertTrue(resultList.size == 2)
        Assert.assertTrue(urlProvider.type == ProviderType.DIKI)
        Assert.assertTrue(resultList.contains(URI.create("https://www.diki.pl/images-common/en/mp3/invert-n.mp3")))
        Assert.assertTrue(resultList.contains(URI.create("https://www.diki.pl/images-common/en/mp3/invert.mp3")))
    }

    @Test
    fun should_receive_two_different_urls_for_double_word_query() {

        val resultList = urlProvider.fetchList("make off")

        Assert.assertTrue(resultList.size == 1)
        Assert.assertTrue(urlProvider.type == ProviderType.DIKI)
        Assert.assertTrue(resultList.contains(URI.create("https://www.diki.pl/images-common/en/mp3/make_off.mp3")))
    }
}
