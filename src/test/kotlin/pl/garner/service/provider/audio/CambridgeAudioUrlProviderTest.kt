package pl.garner.service.provider.audio

import org.junit.Test

import java.net.URI

import org.junit.Assert.assertTrue

class CambridgeAudioUrlProviderTest {

    private val urlProvider = CambridgeAudioUrlProvider()

    @Test
    fun should_receive_two_different_urls_for_single_word_query() {

        val resultList = urlProvider.fetchList("conch")

        assertTrue(resultList.size == 2)
        assertTrue(urlProvider.type == ProviderType.CAMBRIDGE)
        assertTrue(resultList.contains(URI.create("https://dictionary.cambridge.org/media/english/uk_pron/u/ukc/ukcon/ukconce029.mp3")))
        assertTrue(resultList.contains(URI.create("https://dictionary.cambridge.org/media/english/us_pron/u/usc/uscom/uscompr023.mp3")))
    }

    @Test
    fun should_receive_two_different_urls_for_double_word_query() {

        val resultList = urlProvider.fetchList("vicious circle")

        assertTrue(resultList.size == 3)
        assertTrue(urlProvider.type == ProviderType.CAMBRIDGE)
        assertTrue(resultList.contains(URI.create("https://dictionary.cambridge.org/media/english/uk_pron/u/ukc/ukcld/ukcld01584.mp3")))
        assertTrue(resultList.contains(URI.create("https://dictionary.cambridge.org/media/english/us_pron/u/usc/uscld/uscld01584.mp3")))
        assertTrue(resultList.contains(URI.create("https://dictionary.cambridge.org/media/english/us_pron/u/usc/uscld/uscld01788.mp3")))
    }

}
