package pl.garner.service.provider.audio

import org.junit.Assert
import org.junit.Test

import java.net.URI

class VocabularyAudioUrlProviderTest {

    private val urlProvider = VocabularyAudioUrlProvider()

    @Test
    fun should_receive_two_different_urls_for_single_word_query() {

        val resultList = urlProvider.fetchList("invert")

        Assert.assertTrue(resultList.size == 2)
        Assert.assertTrue(urlProvider.type == ProviderType.VOCABULARY)
        Assert.assertTrue(resultList.contains(URI.create("https://audio.vocab.com/1.0/us/I/1DGRY3NALSROH.mp3")))
        Assert.assertTrue(resultList.contains(URI.create("https://audio.vocab.com/1.0/us/I/16SUIYGT0RCWD.mp3")))
    }

    @Test
    fun should_receive_two_different_urls_for_double_word_query() {

        val resultList = urlProvider.fetchList("make off")

        Assert.assertTrue(resultList.size == 1)
        Assert.assertTrue(urlProvider.type == ProviderType.VOCABULARY)
        Assert.assertTrue(resultList.contains(URI.create("https://audio.vocab.com/1.0/us/M/IAWQ8U027PWL.mp3")))
    }

}
