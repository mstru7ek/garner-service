package pl.garner.service.mappper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.garner.service.AddWordDto;
import pl.garner.service.CoordinatesDto;
import pl.garner.service.SiblingDto;
import pl.garner.service.SuggestedWordDto;
import pl.garner.service.repository.Topic;
import pl.garner.service.repository.Word;
import pl.garner.service.WordDto;

import java.util.Collection;
import java.util.List;

@Mapper()
public interface WordsMapper {

    WordsMapper INSTANCE = Mappers.getMapper(WordsMapper.class);

    WordDto wordToWordDto(final Word word);

    @Mapping(target = "topic", ignore = true)
    WordDto wordToWordDtoCapsule(final Word word);

    @Mapping(source = "topic.words", target = "size")
    @Mapping(source = "topic.namespace.id", target = "namespaceId")
    WordDto.TopicDto topicToTopicDto(final Topic topic);

    Word wordDtoToWord(final AddWordDto addWordDto);

    // !!!!!!!!
    @Mapping(source = "word.topic.id", target = "topic.id")
    @Mapping(source = "word.topic.namespace.id", target = "topic.namespaceId")
    @Mapping(source = "siblings", target = "topic.siblings")
    @Mapping(source = "siblings", target = "topic.size")
    WordDto wordWithSiblingsToDto(final Word word, final List<Word> siblings);

    default Integer siblingSize(final Collection<Word> siblings) {
        return siblings.size();
    }

    SiblingDto wordToSiblingDto(final Word word);

    List<SiblingDto> wordToSiblingDto(final List<Word> word);

    CoordinatesDto wordToCoordinateDto(final Word word);

    SuggestedWordDto wordToSuggestedWordDto(final Word word);

}
