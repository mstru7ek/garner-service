package pl.garner.service.mappper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.garner.service.repository.AccountInfo;
import pl.garner.service.PreferencesDto;

@Mapper
public interface PreferencesMapper {

    PreferencesMapper INSTANCE = Mappers.getMapper(PreferencesMapper.class);

    @Mapping(source = "defaultNamespace.id", target = "namespace.id")
    @Mapping(source = "defaultNamespace.name", target = "namespace.name")
    @Mapping(source = "defaultNamespace.active", target = "namespace.active")
    PreferencesDto accountInfoToPreferencesDto(final AccountInfo accountInfo);
}
