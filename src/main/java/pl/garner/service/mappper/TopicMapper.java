package pl.garner.service.mappper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pl.garner.service.repository.Topic;
import pl.garner.service.TopicDto;
import pl.garner.service.TopicWordDto;
import pl.garner.service.repository.Word;

import java.util.List;

@Mapper
public interface TopicMapper {

    TopicMapper INSTANCE = Mappers.getMapper(TopicMapper.class);

    TopicDto topicToTopicDto(final Topic topic);

    @Mapping(source = "word.topic.id", target = "topic.id")
    @Mapping(source = "siblingsRefs", target = "topic.siblingsRefs")
    TopicWordDto wordToTopicWordDto(final Word word, List<Long> siblingsRefs);

    @Mapping(source = "topic.id", target = "id")
    @Mapping(source = "wordsNames", target = "wordsNames")
    @Mapping(source = "wordNamesSize", target = "size")
    TopicDto topicToTopicDto(final Topic topic, final List<String> wordsNames, final Integer wordNamesSize);
}
