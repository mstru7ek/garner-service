package pl.garner.service.mappper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.garner.service.repository.Namespace;
import pl.garner.service.NamespaceDto;

@Mapper
public interface NamespaceMapper {

    NamespaceMapper INSTANCE = Mappers.getMapper(NamespaceMapper.class);

    NamespaceDto namespaceToDto(final Namespace source);
}
