package pl.garner.service.mappper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.garner.service.repository.AudioFile;
import pl.garner.service.AudioFileDto;
import pl.garner.service.CreateAudioFileDto;

import java.util.List;

@Mapper
public interface MediaMapper {

    MediaMapper INSTANCE = Mappers.getMapper(MediaMapper.class);

    AudioFileDto mediaToMediaDto(final AudioFile source);
    List<AudioFileDto> mediaToMediaDto(final List<AudioFile> source);
    AudioFile addMediaDtoToMedia(final CreateAudioFileDto source);
}
