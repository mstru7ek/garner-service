#!/bin/sh

echo "The application will start in ${GARNER_SLEEP}s..." && sleep ${GARNER_SLEEP}
exec java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "${HOME}/garner-service.war" "$@"
