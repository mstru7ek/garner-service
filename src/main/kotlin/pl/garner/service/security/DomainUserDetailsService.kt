package pl.garner.service.security

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator
import org.slf4j.LoggerFactory
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.repository.User
import pl.garner.service.repository.UserRepository
import java.util.*
import org.springframework.security.core.userdetails.User as SecurityUser

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
class DomainUserDetailsService(private val userRepository: UserRepository) : UserDetailsService {

    @Transactional
    override fun loadUserByUsername(login: String): UserDetails {
        log.debug("Authenticating {}", login)

        if (EmailValidator().isValid(login, null)) {
            return userRepository.findOneWithAuthoritiesByEmail(login)
                    .map { user -> createSpringSecurityUser(login, user) }
                    .orElseThrow {
                        UsernameNotFoundException("User with email $login was not found in the database")
                    }
        }

        val lowercaseLogin = login.toLowerCase(Locale.ENGLISH)
        return userRepository.findOneWithAuthoritiesByLogin(lowercaseLogin)
                .map { user -> createSpringSecurityUser(lowercaseLogin, user) }
                .orElseThrow {
                    UsernameNotFoundException("User $lowercaseLogin was not found in the database")
                }
    }

    private fun createSpringSecurityUser(lowercaseLogin: String, user: User): SecurityUser {
        if (!user.activated) {
            throw UserNotActivatedException("User $lowercaseLogin was not activated")
        }
        return SecurityUser(
                user.login,
                user.password,
                user.authorities.map { authority -> SimpleGrantedAuthority(authority.name) }
        )
    }

    companion object {
        private val log = LoggerFactory.getLogger(DomainUserDetailsService::class.java)
    }
}

/**
 * This exception is thrown in case of a not activated user trying to authenticate.
 */
class UserNotActivatedException(message: String) : AuthenticationException(message)
