package pl.garner.service.security

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails

/**
 * Utility class for Spring Security.
 */
object SecurityUtils {

    /**
     * Get the login of the current user.
     *
     * @return the login of the current user
     */
    fun currentUserLogin(): String {
        val principal = SecurityContextHolder.getContext().authentication.principal
        return when (principal) {
            is UserDetails -> principal.username
            is String -> principal
            else -> throw IllegalStateException("Unauthorized access")
        }
    }

    /**
     * Get the JWT of the current user.
     *
     * @return the JWT of the current user
     */
    fun currentUserJWT(): String? {
        return SecurityContextHolder.getContext().authentication?.credentials as? String
    }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise
     */
    fun isAuthenticated(): Boolean {
        return SecurityContextHolder.getContext().authentication?.authorities
                ?.none { grantedAuthority -> AuthoritiesConstants.ANONYMOUS == grantedAuthority.authority }
                ?: false
    }

    /**
     * If the current user has a specific authority (security role).
     *
     *
     * The name of this method comes from the isUserInRole() method in the Servlet API
     *
     * @param authority the authority to check
     * @return true if the current user has the authority, false otherwise
     */
    fun isCurrentUserInRole(authority: String): Boolean {
        return SecurityContextHolder.getContext()
                .authentication
                ?.authorities
                ?.any { grantedAuthority -> grantedAuthority.authority == authority }
                ?: false
    }
}

/**
 * Constants for Spring Security authorities.
 */
object AuthoritiesConstants {
    const val ADMIN = "ROLE_ADMIN"
    const val USER = "ROLE_USER"
    const val ANONYMOUS = "ROLE_ANONYMOUS"
}