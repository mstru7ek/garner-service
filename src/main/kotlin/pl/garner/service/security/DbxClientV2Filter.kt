package pl.garner.service.security

import com.dropbox.core.DbxRequestConfig
import com.dropbox.core.v2.DbxClientV2
import org.slf4j.LoggerFactory
import org.springframework.web.filter.GenericFilterBean
import pl.garner.service.dropbox.DbxAccountService
import pl.garner.service.dropbox.DbxClientV2Holder
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse

class DbxClientV2Filter(private val dbxAccountService: DbxAccountService) : GenericFilterBean() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val userPreferences = dbxAccountService.userPreferences()
        val dbxAccessToken = userPreferences.dbxAccessToken

        log.info("userPreferences = $userPreferences")
        log.info("User dbxAccessToken is ok ? ${dbxAccessToken.isNotBlank()}")

        DbxClientV2Holder.set(DbxClientV2(REQUEST_CONFIG, dbxAccessToken))
        try {
            chain.doFilter(request, response)
        } finally {
            DbxClientV2Holder.clear()
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(DbxClientV2Filter::class.java)
        val REQUEST_CONFIG: DbxRequestConfig = DbxRequestConfig.newBuilder("examples-authorize").withAutoRetryEnabled().build()
    }
}
