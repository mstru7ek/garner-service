package pl.garner.service.security.jwt

import io.github.jhipster.config.JHipsterProperties
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component
import java.nio.charset.StandardCharsets
import java.security.Key
import java.util.*
import javax.annotation.PostConstruct

@Component
class TokenProvider(private val jHipsterProperties: JHipsterProperties) {

    private lateinit var key: Key
    private var tokenValidityInMilliseconds: Long = 0
    private var tokenValidityInMillisecondsForRememberMe: Long = 0

    @PostConstruct
    fun init() {
        val jwt = jHipsterProperties.security.authentication.jwt

        val keyBytes: ByteArray = if (jwt.secret?.isNotEmpty() ?: false) {
            jwt.secret.toByteArray(StandardCharsets.UTF_8)
        } else {
            Decoders.BASE64.decode(jwt.base64Secret)
        }

        this.key = Keys.hmacShaKeyFor(keyBytes)
        this.tokenValidityInMilliseconds = DUR_1_SEC * jwt.tokenValidityInSeconds
        this.tokenValidityInMillisecondsForRememberMe = DUR_1_SEC * jwt.tokenValidityInSecondsForRememberMe
    }

    fun createToken(authentication: Authentication, rememberMe: Boolean): String {
        val authorities = authentication.authorities
                .map { grantedAuthority -> grantedAuthority.authority }
                .joinToString(separator = ",")

        val now = Date().time
        val validity = when {
            rememberMe -> Date(now + this.tokenValidityInMillisecondsForRememberMe)
            else -> Date(now + this.tokenValidityInMilliseconds)
        }

        return Jwts.builder()
                .setSubject(authentication.name)
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(validity)
                .compact()
    }

    fun getAuthentication(token: String): Authentication {
        val claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).body

        val authorities = claims[AUTHORITIES_KEY].toString().split(",").map { SimpleGrantedAuthority(it) }
        val principal = User(claims.subject, "", authorities)
        return UsernamePasswordAuthenticationToken(principal, token, authorities)
    }

    fun validateToken(authToken: String): Boolean {
        if (authToken.isEmpty()){
            return false
        }
        try {
            Jwts.parser().setSigningKey(key).parseClaimsJws(authToken)
            return true
        } catch (e: JwtException) {
            log.error("JWT Invalid token", e)
        } catch (e: IllegalArgumentException) {
            log.error("JWT token compact of handler are invalid trace:", e)
        }
        return false
    }

    companion object {
        private val log = LoggerFactory.getLogger(TokenProvider::class.java)

        const val AUTHORITIES_KEY = "auth"
        const val DUR_1_SEC = 1000
    }
}
