package pl.garner.service.security.jwt

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.util.StringUtils
import org.springframework.web.filter.GenericFilterBean
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

/**
 * Filters incoming requests and installs a Spring Security principal if a header corresponding to a valid user is
 * found.
 */
class JWTFilter(private val tokenProvider: TokenProvider) : GenericFilterBean() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain) {
        val jwt = resolveToken(servletRequest as HttpServletRequest)
        if (this.tokenProvider.validateToken(jwt)) {
            val authentication = this.tokenProvider.getAuthentication(jwt)
            SecurityContextHolder.getContext().authentication = authentication
        }
        filterChain.doFilter(servletRequest, servletResponse)
    }

    private fun resolveToken(request: HttpServletRequest): String {
        val bearerToken = request.getHeader(AUTHORIZATION_HEADER)
        val startWithToken = StringUtils.hasText(bearerToken) && bearerToken.startsWith(AUTH_BEARER)
        return when {
            startWithToken -> bearerToken.substring(AUTH_BEARER.length, bearerToken.length)
            else -> ""
        }
    }

    companion object {
        const val AUTHORIZATION_HEADER = "Authorization"
        const val AUTH_BEARER = "Bearer "
    }
}
