package pl.garner.service.dropbox

import com.dropbox.core.DbxSessionStore
import org.springframework.stereotype.Component
import org.springframework.web.context.annotation.SessionScope

import java.io.Serializable

@Component
@SessionScope
class DbxRepositorySessionStore : DbxSessionStore, Serializable {

    private var dbxCsrfToken: String? = null

    val key: String
        get() = DBX_CSRF_KEY

    override fun get(): String? {
        return dbxCsrfToken
    }

    override fun set(value: String) {
        this.dbxCsrfToken = value
    }

    override fun clear() {
        // because of session scope
    }

    companion object {
        private const val serialVersionUID = -5295446925087353950L
        private const val DBX_CSRF_KEY = "DBX_CSRF_KEY"
    }
}
