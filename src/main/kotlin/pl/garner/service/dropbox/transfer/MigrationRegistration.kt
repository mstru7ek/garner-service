package pl.garner.service.dropbox.transfer

class MigrationRegistration(val migrationId: String, val url: String)
