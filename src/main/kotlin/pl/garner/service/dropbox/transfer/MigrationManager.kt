package pl.garner.service.dropbox.transfer

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import pl.garner.service.FileMetadataDto
import pl.garner.service.dropbox.DbxServiceException
import pl.garner.service.dropbox.DbxStorageService
import pl.garner.service.dropbox.FileService

/**
 * Synchronization Manager Worker.
 */
@Service
class MigrationManager(
        private val fileService: FileService,
        private val dbxStorageService: DbxStorageService
) {

    fun invoke(registration: MigrationRegistration): String {

        val migrationUrl = registration.url

        val metadata = fileService.findByUrl(migrationUrl)

        val migrationJob = dbxStorageService.uploadFile(metadata.name, migrationUrl)

        // Confirm upload status
        val jobStatus = dbxStorageService.confirmJobStatus(migrationJob)

        if (!jobStatus.isComplete) {
            throw DbxServiceException("Failed - job status , url : $migrationUrl")
        }

        val sharedLink = dbxStorageService.createSharedLink(jobStatus.completeValue)

        val newFileMetadataDto = FileMetadataDto.valid(
                sharedLink.url,
                sharedLink.id,
                sharedLink.name,
                metadata.type)

        // save new metadata
        val sharedUrl = fileService.saveFileMetadata(
                metadata.namespaceId,
                metadata.wordId,
                metadata.wordName,
                newFileMetadataDto)

        // remove previous metadata
        fileService.deleteFile(metadata.namespaceId, metadata.url)

        return sharedUrl
    }

    companion object {
        private val log = LoggerFactory.getLogger(MigrationManager::class.java)
    }
}
