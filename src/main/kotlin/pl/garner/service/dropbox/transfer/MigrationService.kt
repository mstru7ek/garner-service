package pl.garner.service.dropbox.transfer

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import pl.garner.service.dropbox.AccountContext
import pl.garner.service.dropbox.DbxClientV2Holder
import pl.garner.service.dropbox.FileService
import pl.garner.service.repository.FileMetadata
import pl.garner.service.security.SecurityUtils
import java.util.*
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicBoolean
import javax.annotation.PostConstruct

@Service
class MigrationService(private val migrationManager: MigrationManager, private val fileService: FileService) {

    private val contexts = ConcurrentHashMap<String, MigrationContext>()
    private val subjects = ConcurrentHashMap<String, MigrationSubject>()
    private val migrationJobs = LinkedBlockingDeque<MigrationRegistration>()
    private val workers = CopyOnWriteArrayList<Thread>()
    private val executor = Executors.newFixedThreadPool(MAX_WATCHERS)

    private var stop = AtomicBoolean(false)

    @PostConstruct
    fun init() {
        for (i in 0 until MAX_WORKERS) {
            val worker = Thread(
                    /**
                     * main worker loop
                     *
                     */
                    Runnable { this.mainWorkerLoop() }
            )
            worker.name = WORKER_PREFIX_NAME + i
            worker.uncaughtExceptionHandler = Thread.UncaughtExceptionHandler { thread, e ->
                this.getUncaughtExceptionHandler(thread, e)
            }
            workers.add(worker)
            worker.start()
        }
    }

    /**
     * @param migrationId the same as HTTP Session ID
     */
    fun startMigration(migrationId: String): Boolean {
        log.info("Start migration [ migrationId: $migrationId ]")
        // prepare migration registrations
        val filesMetadata = this.fileService.findAll()
        val login = SecurityUtils.currentUserLogin()

        val existsLoginMigration = existsLoginMigrationContext(login)
        if (existsLoginMigration) {
            log.warn("User migration context exists [ login: $login ]")
            return false
        }
        // register
        val migrationRegistrations = buildRegistrations(migrationId, filesMetadata)

        if(migrationRegistrations.size != 0 ) {
            contexts[migrationId] = MigrationContext(filesMetadata.size, login, DbxClientV2Holder.instance)
            subjects[migrationId] = MigrationSubject()
            publish(migrationRegistrations)
        }
        log.info("Published migrations , size = $migrationId, [ migrationId = ${migrationRegistrations.size} ]")
        return true
    }

    fun findMigrationSubject(migrationId: String): MigrationSubject? {
        return subjects[migrationId]
    }

    fun stop() {
        this.stop.set(true)
    }

    /**
     * Check if login migration context exists.
     * @param login
     * @return
     */
    private fun existsLoginMigrationContext(login: String?): Boolean = contexts.values.any { it.login == login }

    /**
     * Recreate killed worker because of thrown exception.
     * @param worker
     * @param e
     */
    private fun getUncaughtExceptionHandler(worker: Thread, e: Throwable) {
        // remove stale worker
        this.workers.remove(worker)

        log.error("Worker killed : ")
        log.error("Worker exception , [ name:id = ${worker.name}:${worker.id} ]", e)

        val workerId: String = worker.name.split("-")[2]
        val resurrected = Thread(
                /**
                 * main worker loop
                 *
                 */
                Runnable { this.mainWorkerLoop() }
        )
        resurrected.name = WORKER_PREFIX_NAME + workerId
        resurrected.uncaughtExceptionHandler = Thread.UncaughtExceptionHandler { thread, exp ->
            this.getUncaughtExceptionHandler(thread, exp)
        }
        workers.add(resurrected)
        resurrected.start()
    }

    /**
     * Main worker loop responsible for processing migration requests.
     */
    private fun mainWorkerLoop() {
        while (!stop.get()) {

            if (Thread.interrupted()) {
                log.info("Worker Interrupted")
                return
            }

            val migrationJob: MigrationRegistration = try {
                migrationJobs.take()
            } catch (e: InterruptedException) {
                log.error("Main Worker Loop interrupted .. empty", e)
                continue
            }

            val migrationId = migrationJob.migrationId
            val context = contexts[migrationId] ?: throw IllegalStateException("context is null, migrationId = $migrationId")
            val subject = subjects[migrationId] ?: throw IllegalStateException("subject is null, migrationId = $migrationId")

            val isCompleted = processSingleFileMigration(migrationJob, context)
            publishStatus(context, subject)
            if (isCompleted) {
                // notify subscribers for websocket
                subject.isCompleted = true
                closeMigrationContext(migrationId, context, subject)
            }
        }
        log.info("Worker Stopped")
    }

    /**
     * Send migration status onto subject.
     * @param context
     * @param subject
     */
    private fun publishStatus(context: MigrationContext, subject: MigrationSubject) {
        val statusMessage = migrationStatus(context)
        // publish migration state
        subject.messages.offer(statusMessage)
    }

    /**
     * Process single file migration.
     * @param migrationJob
     * @param context
     * @return
     */
    private fun processSingleFileMigration(migrationJob: MigrationRegistration, context: MigrationContext): Boolean {
        /**
         * setup Thread security context.
         *
         */
        DbxClientV2Holder.set(context.dbxClientV2)
        AccountContext.setWSLogin(context.login)
        return try {
            val newSharedUrl = migrationManager.invoke(migrationJob)
            log.info("OK [ mId: ${migrationJob.migrationId}, url: $newSharedUrl ] ")
            context.addOk()
        } catch (e: Exception) {
            log.error("File migration error", e)
            log.info("FAILED [ mId: ${migrationJob.migrationId}, url: ${migrationJob.url} ] ")
            context.addFail()
        } finally {
            AccountContext.clearWSLogin()
            DbxClientV2Holder.clear()
        }
    }

    /**
     * Remove migration context and migration subject.
     * @param migrationId
     * @param context
     * @param subject
     */
    private fun closeMigrationContext(migrationId: String, context: MigrationContext, subject: MigrationSubject) {
        subject.messages.offer(MSG_COMPLETED)
        contexts.remove(migrationId, context)
        subjects.remove(migrationId, subject)
    }

    /**
     * Get current migration state.
     * @param context
     * @return
     */
    private fun migrationStatus(context: MigrationContext): String {
        synchronized(context) {
            val ok = context.ok
            val fail = context.fail
            val total = context.total
            return "Status, OK = [$ok], FAIL = [$fail], TOTAL = [$total]"
        }
    }

    private fun buildRegistrations(migrationId: String, filesMetadata: List<FileMetadata>): List<MigrationRegistration> {
        val migrationRegistrations = ArrayList<MigrationRegistration>()
        for (fileMetadata in filesMetadata) {
            migrationRegistrations.add(MigrationRegistration(migrationId, fileMetadata.url))
        }
        return migrationRegistrations
    }

    /**
     * Register all migrations at one shot.
     * @param requests
     */
    @Synchronized
    private fun publish(requests: List<MigrationRegistration>) {
        for (migrationRegistration in requests) {
            migrationJobs.offer(migrationRegistration)
        }
    }

    fun buildStatusEmitter(migrationId: String): SseEmitter {
        val emitter = SseEmitter()
        executor.execute(Runnable {
            val subject = findMigrationSubject(migrationId)
            if(subject == null) {
                publishEmptySubject(emitter)
            } else {
                publishStatus(subject, emitter, migrationId)
            }
        })
        return emitter;
    }

    private fun publishStatus(subject: MigrationSubject, emitter: SseEmitter, migrationId: String) {
        while (!subject.isCompleted) {
            val status = subject.messages.poll(MSG_POLL_TIMEOUT.toLong(), TimeUnit.MILLISECONDS) ?: continue
            val event = sseEvent(status)
            emitter.send(event)
        }
        log.info("Migration completed [ migrationId: {} ]", migrationId)

        val closeEvent = sseCloseEvent(MSG_MIGRATION_COMPLETED)
        emitter.send(closeEvent)
        emitter.complete()
    }

    private fun publishEmptySubject(emitter: SseEmitter) {
        val closeEvent = sseCloseEvent(MSG_ERROR_NO_MIGRATION_QUEUE)
        emitter.send(closeEvent)
        emitter.complete()
    }

    private fun sseEvent(text: String) = SseEmitter.event().data(text)

    private fun sseCloseEvent(text: String) = SseEmitter.event().name(EVENT_NAME_CLOSE).data(text)

    companion object {
        private val log = LoggerFactory.getLogger(MigrationService::class.java)

        private const val MAX_WORKERS = 3
        private const val MSG_COMPLETED = "COMPLETED"
        private const val WORKER_PREFIX_NAME = "migration-worker-"

        private const val MAX_WATCHERS = 3

        const val EVENT_NAME_CLOSE = "close"
        const val MSG_ERROR_NO_MIGRATION_QUEUE = "No migration queue !"
        const val MSG_MIGRATION_COMPLETED = "Account migration completed !"
        const val MSG_POLL_TIMEOUT = 5000
    }
}
