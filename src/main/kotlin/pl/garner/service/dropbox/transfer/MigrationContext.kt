package pl.garner.service.dropbox.transfer

import com.dropbox.core.v2.DbxClientV2

class MigrationContext(val total: Int, val login: String, val dbxClientV2: DbxClientV2) {

    @Volatile
    @get:Synchronized
    var ok: Int = 0

    @Volatile
    @get:Synchronized
    var fail: Int = 0

    private val isCompleted: Boolean
        @Synchronized get() = ok + fail == total

    @Synchronized
    fun addOk(): Boolean {
        this.ok = this.ok + 1
        return isCompleted
    }

    @Synchronized
    fun addFail(): Boolean {
        this.fail = this.fail + 1
        return isCompleted
    }
}
