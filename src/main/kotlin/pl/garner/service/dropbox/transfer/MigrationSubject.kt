package pl.garner.service.dropbox.transfer

import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

class MigrationSubject {

    val messages: BlockingQueue<String> = LinkedBlockingQueue()

    @Volatile
    @get:Synchronized
    @set:Synchronized
    var isCompleted = false
}
