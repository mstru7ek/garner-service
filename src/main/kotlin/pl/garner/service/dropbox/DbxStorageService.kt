package pl.garner.service.dropbox

import com.dropbox.core.v2.files.FileMetadata
import com.dropbox.core.v2.files.SaveUrlJobStatus
import com.dropbox.core.v2.files.SaveUrlResult
import com.dropbox.core.v2.files.WriteMode
import com.dropbox.core.v2.sharing.RequestedVisibility
import com.dropbox.core.v2.sharing.SharedLinkMetadata
import com.dropbox.core.v2.sharing.SharedLinkSettings
import net.jodah.failsafe.CircuitBreaker
import net.jodah.failsafe.Failsafe
import net.jodah.failsafe.Fallback
import net.jodah.failsafe.RetryPolicy
import net.jodah.failsafe.function.CheckedConsumer
import net.jodah.failsafe.function.CheckedSupplier
import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.time.Duration
import java.time.Instant
import java.util.*

@Service
class DbxStorageService {

    fun uploadFile(inputStream: InputStream, fileName: String): FileMetadata {
        val dropboxPath = "/${filePrefix(fileName)}.${Instant.now().epochSecond}${fileExt(fileName)}"

        val dbxClientUpload = DbxClientV2Holder.instance.files()
                .uploadBuilder(dropboxPath)
                .withMode(WriteMode.ADD)
                .withClientModified(Date())

        val fallback = withFallback(ERROR_INPUT_STREAM_FAILED)
        return Failsafe.with(fallback, retryPolicy, circuitBreaker).get(CheckedSupplier {
            val buffer = ByteArrayOutputStream()
            IOUtils.copy(inputStream, buffer)
            dbxClientUpload.uploadAndFinish(ByteArrayInputStream(buffer.toByteArray()))
        })
    }

    fun createSharedLink(metadata: FileMetadata): SharedLinkMetadata {
        val fallback = withFallback(ERROR_CREATE_SHARED_LINK)
        return Failsafe.with(fallback, retryPolicy, circuitBreaker).get(CheckedSupplier {
            DbxClientV2Holder.instance.sharing().createSharedLinkWithSettings(metadata.pathDisplay,sharedLinkSettings)
        })
    }

    fun confirmJobStatus(migrationJob: SaveUrlResult): SaveUrlJobStatus {
        val fallback = withFallback(ERROR_FAILED_JOB_STATUS_FETCH)
        return Failsafe.with(fallback, retryPolicy, circuitBreaker).get(CheckedSupplier {
            DbxClientV2Holder.instance.files().saveUrlCheckJobStatus(migrationJob.asyncJobIdValue)
        })
    }

    fun uploadFile(filePath: String, fileUrl: String): SaveUrlResult {
        val fallback = withFallback(ERROR_FILE_UPLOAD)
        return Failsafe.with(fallback, retryPolicy, circuitBreaker).get(CheckedSupplier {
            DbxClientV2Holder.instance.files().saveUrl("/$filePath", fileUrl)
        })
    }

    companion object {
        private val log = LoggerFactory.getLogger(DbxStorageService::class.java)

        const val ERROR_FAILED_JOB_STATUS_FETCH = "FAILED to fetch job status"
        const val ERROR_CREATE_SHARED_LINK = "Error creating shared link"
        const val ERROR_INPUT_STREAM_FAILED = "Input stream failed"
        const val ERROR_FILE_UPLOAD = "Error file upload file"

        private const val MAX_REQUEST_RETRIES = 3
        private val retryPolicy = RetryPolicy<Any>()
                .handle(Exception::class.java)
                .withMaxRetries(MAX_REQUEST_RETRIES)
                .onRetry { event -> log.warn(" >>) On Retry - $event") }

        private val circuitBreaker: CircuitBreaker<Any> = CircuitBreaker<Any>()
                .handle(Exception::class.java)
                .withFailureThreshold(3)
                .withSuccessThreshold(1)
                .withDelay(Duration.ofSeconds(1))
                .onOpen { log.error(" >>/ Circuit Breaker - OPEN ") }

        private val sharedLinkSettings
            get() = SharedLinkSettings.newBuilder()
                    .withRequestedVisibility(RequestedVisibility.PUBLIC)
                    .build()

        fun fileExt(filePath: String): String {
            val pos = filePath.lastIndexOf('.')
            return if (pos != -1) filePath.substring(pos) else filePath
        }

        fun filePrefix(filePath: String): String {
            val pos = filePath.lastIndexOf('.')
            return if (pos != -1) filePath.substring(0, pos) else filePath
        }

        fun withFallback(message: String): Fallback<Any> {
            return Fallback.of(CheckedConsumer { failure ->
                throw DbxServiceException(message, failure.lastFailure)
            })
        }
    }
}
