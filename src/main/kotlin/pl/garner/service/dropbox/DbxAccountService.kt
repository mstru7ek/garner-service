package pl.garner.service.dropbox

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.repository.DbxAccountInfo
import pl.garner.service.repository.DbxAccountInfoRepository

@Service
@Transactional
class DbxAccountService(private val dbxAccountInfoRepository: DbxAccountInfoRepository) {

    fun userPreferences(): DbxAccountInfo =
            dbxAccountInfoRepository.findByLogin(AccountContext.login).orElse(DbxAccountInfo.EMPTY)

    fun createAccountDbxOauth(dbxUserId: String, dbxAccountId: String, dbxAccessToken: String,
                              dbxCsrfToken: String) {
        dbxAccountInfoRepository.findByLogin(AccountContext.login).ifPresent { dbxAccount ->
            log.info("Authorization for dbx access exists, user: ${AccountContext.login}")
            dbxAccountInfoRepository.delete(dbxAccount)
        }

        val accountInfo = DbxAccountInfo(null, AccountContext.login, dbxCsrfToken, dbxUserId, dbxAccountId,
                dbxAccessToken)

        dbxAccountInfoRepository.save(accountInfo)

        val confirmationMsg = """
            | Authorization completed .
            | User: ${AccountContext.login}
            | User ID: $dbxUserId
            | Account ID: $dbxAccountId
            | Access Token: $dbxAccessToken
            | CSRF Token: $dbxCsrfToken
        """.trimMargin()

        log.info(confirmationMsg)
    }

    fun removeAccountDbxOauthState() {
        dbxAccountInfoRepository.removeByLogin(AccountContext.login)
        log.trace("User Dbx account authorization cleared")
    }

    companion object {
        private val log = LoggerFactory.getLogger(DbxAccountService::class.java)
    }
}
