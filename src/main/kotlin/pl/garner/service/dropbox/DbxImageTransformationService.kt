package pl.garner.service.dropbox

import com.twelvemonkeys.image.ResampleOp
import okhttp3.MediaType
import org.springframework.stereotype.Service
import pl.garner.service.config.ApplicationProperties
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import javax.imageio.ImageIO

@Service
class DbxImageTransformationService(private val applicationProperties: ApplicationProperties) {

    fun transform(fileInputStream: InputStream, mediaType: MediaType): ByteArrayInputStream {
        try {
            fileInputStream.use { inputStream ->
                var buffer = ImageIO.read(inputStream) ?: throw TransformationReaderError

                if (MEDIA_TYPE_PNG == mediaType.subtype()) {
                    val copyInRGB = BufferedImage(buffer.width, buffer.height, BufferedImage.TYPE_INT_RGB)
                    val graphics2D = copyInRGB.createGraphics()
                    graphics2D.drawImage(buffer, 0, 0, Color.BLACK, null)
                    buffer = copyInRGB
                }

                // new width/height
                val scaledWidth = applicationProperties.imageFixedWidthPx
                val scaledHeight = (buffer.height.toDouble() / buffer.width * scaledWidth).toInt()

                val resampleOp = ResampleOp(scaledWidth, scaledHeight, ResampleOp.FILTER_LANCZOS)
                val output = resampleOp.filter(buffer, null)

                val transformedBuffer = ByteArrayOutputStream()

                if (!ImageIO.write(output, OUTPUT_EXTENSION, transformedBuffer))
                    throw TransformationWriterError

                return ByteArrayInputStream(transformedBuffer.toByteArray())
            }
        } catch (e: IOException) {
            throw DbxServiceException("Transformation error", e)
        }
    }

    companion object {
        private const val MEDIA_TYPE_PNG = "png"
        private const val OUTPUT_EXTENSION = "jpeg"

        private val TransformationReaderError = DbxServiceException("Transformation image reader error")
        private val TransformationWriterError = DbxServiceException("Transformation image write error")
    }
}
