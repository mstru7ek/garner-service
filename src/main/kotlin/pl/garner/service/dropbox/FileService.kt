package pl.garner.service.dropbox

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.FileMetadataDto
import pl.garner.service.repository.FileMetadata
import pl.garner.service.repository.FileMetadataRepository
import java.time.LocalDateTime

@Service
@Transactional
class FileService(private val fileMetadataRepository: FileMetadataRepository) {

    fun saveFileMetadata(namespaceId: Long, wordId: Long, wordName: String, fileMetadataDto: FileMetadataDto): String {
        val fileMetadata = toImageEntity(namespaceId, wordId, wordName, fileMetadataDto)
        val saved = fileMetadataRepository.save(fileMetadata)
        return saved.url
    }

    @Transactional(readOnly = true)
    fun findAll(namespaceId: Long, wordId: Long): List<FileMetadata> {
        return fileMetadataRepository.findAllByWordId(AccountContext.login, namespaceId, wordId)
    }

    @Transactional(readOnly = true)
    fun findAll(): List<FileMetadata> {
        return fileMetadataRepository.findAllByLogin(AccountContext.login)
    }

    fun lookBack(namespaceId: Long, pageable: Pageable): Page<FileMetadata> {
        return fileMetadataRepository.findAllInNamespace(AccountContext.login, namespaceId, pageable)
    }

    fun deleteFile(namespaceId: Long, url: String) {
        fileMetadataRepository.deleteByUrl(AccountContext.login, namespaceId, url)
    }

    @Transactional(readOnly = true)
    fun findByUrl(url: String): FileMetadata {
        return fileMetadataRepository.findById(url)
                .orElseThrow { throw DbxServiceException("Invalid file metadata url = $url") }
    }

    fun deleteAllFiles(namespaceId: Long, wordId: Long) {
        fileMetadataRepository.deleteAllFiles(AccountContext.login, namespaceId, wordId)
    }

    private fun toImageEntity(namespaceId: Long, wordId: Long, wordName: String,
                              fileMetadataDto: FileMetadataDto): FileMetadata {
        return FileMetadata(
                fileMetadataDto.url.replaceFirst("?dl=0", "?dl=1"),
                fileMetadataDto.id,
                fileMetadataDto.name,
                fileMetadataDto.type,
                AccountContext.login,
                namespaceId,
                wordId,
                wordName,
                LocalDateTime.now()
        )
    }
}
