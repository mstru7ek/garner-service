package pl.garner.service.dropbox

import okhttp3.MediaType
import java.io.ByteArrayInputStream

data class DownloadFileResource(
        val fileName: String,
        val mediaType: MediaType,
        val downloadInputStream: ByteArrayInputStream
)
