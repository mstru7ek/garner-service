package pl.garner.service.dropbox

import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.commons.io.FilenameUtils
import org.apache.commons.io.IOUtils
import org.springframework.stereotype.Service
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException

@Service
class DownloadService {

    private var client: OkHttpClient = OkHttpClient()

    fun download(fileUrl: String): DownloadFileResource {

        val request = Request.Builder().url(fileUrl).build()

        val baseName = FilenameUtils.getBaseName(fileUrl)
        val extension = FilenameUtils.getExtension(fileUrl)
        val fileName = "$baseName.$extension"

        try {
            client.newCall(request).execute().use { response ->

                val contentType = response.header(HEADER_CONTENT_TYPE)
                        ?: throw DbxServiceException("File content type error")

                if (response.body() == null) {
                    throw DbxServiceException("Response body error")
                }
                val mediaType = MediaType.get(contentType)

                val buffer = ByteArrayOutputStream()
                val responseInputStream = response.body()!!.byteStream()
                IOUtils.copy(responseInputStream, buffer)
                val downloadInputStream = ByteArrayInputStream(buffer.toByteArray())

                return DownloadFileResource(fileName, mediaType, downloadInputStream)
            }
        } catch (e: IOException) {
            throw DbxServiceException("Download file error", e)
        }
    }

    companion object {
        private const val HEADER_CONTENT_TYPE = "Content-Type"
    }
}
