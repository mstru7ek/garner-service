package pl.garner.service.dropbox

import com.dropbox.core.DbxAppInfo
import com.dropbox.core.DbxException
import com.dropbox.core.DbxRequestConfig
import com.dropbox.core.DbxWebAuth
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import pl.garner.service.DbxAuthenticationDto
import pl.garner.service.DbxAuthorizeUri
import pl.garner.service.config.ApplicationProperties
import javax.servlet.http.HttpServletRequest

@Service
class DbxRegistryService(
        private val env: Environment,
        private val applicationProperties: ApplicationProperties,
        private val dbxAppInfo: DbxAppInfo,
        private val accountService: DbxAccountService,
        private val dbxRepositorySessionStore: DbxRepositorySessionStore
) {

    private lateinit var dbxWebAuth: DbxWebAuth

    private val redirectUri: String
        get() = buildRedirectURI()

    val isValidAccessToken: Boolean
        get () {
            return try {
                DbxClientV2Holder.instance.users().currentAccount
                true
            } catch (e: DbxException) {
                log.warn("Authorization Failed ! requestId: ${e.requestId}", e)
                false
            }
        }

    init {
        initDbxWebAuth()
    }

    private fun initDbxWebAuth() {
        dbxWebAuth = DbxWebAuth(REQUEST_CONFIG, dbxAppInfo)
    }

    fun retrieveAuthorizeUrl(servletRequest: HttpServletRequest): DbxAuthorizeUri {
        val redirectUri = redirectUri
        val webAuthRequest = DbxWebAuth.newRequestBuilder()
                .withRedirectUri(redirectUri, dbxRepositorySessionStore)
                .build()
        val authorizeUrl = dbxWebAuth.authorize(webAuthRequest)
        return DbxAuthorizeUri(authorizeUrl)
    }

    fun confirmAccessGranted(parameterMap: Map<String, Array<String>>): DbxAuthenticationDto {
        try {
            val authFinish = dbxWebAuth.finishFromRedirect(redirectUri, dbxRepositorySessionStore, parameterMap)

            val dbxCsrfToken = dbxRepositorySessionStore.get() ?: throw CsrfTokenNullError
            accountService.createAccountDbxOauth(
                    authFinish.userId,
                    authFinish.accountId,
                    authFinish.accessToken,
                    dbxCsrfToken)
            return DbxAuthenticationDto.completed()
        } catch (e: DbxWebAuth.Exception) {
            throw DbxAuthorizationException("Web Authentication Failed !", e)
        } catch (e: DbxException) {
            throw DbxAuthorizationException("Authorization Failed !", e)
        }
    }

    fun revokeAuthorizationToken() {
        try {
            DbxClientV2Holder.instance.auth().tokenRevoke()
            accountService.removeAccountDbxOauthState()
        } catch (e: DbxException) {
            throw DbxAuthorizationException("Revoke token action Failed!", e)
        }
    }

    private fun buildRedirectURI(): String {
        val redirectUriPath = applicationProperties.dropbox.redirect.uri
        val hostUrl = env.getProperty(ENV_HOST_URL)!!
        val redirectUri = hostUrl + redirectUriPath
        log.info("Redirect URI: $redirectUri")
        return redirectUri
    }

    companion object {
        private val log = LoggerFactory.getLogger(DbxRegistryService::class.java)

        private const val ENV_HOST_URL = "HOST_URL"
        private const val MAX_CONNECTION_RETRY = 3
        private const val DBX_CLIENT_IDENTIFIER = "garner-oauth"

        private val CsrfTokenNullError = DbxAuthorizationException("CSRF Token is null")

        private val REQUEST_CONFIG = DbxRequestConfig.newBuilder(DBX_CLIENT_IDENTIFIER)
                .withAutoRetryEnabled(MAX_CONNECTION_RETRY)
                .build()
    }
}
