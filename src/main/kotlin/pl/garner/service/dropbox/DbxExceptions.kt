package pl.garner.service.dropbox

class DbxAuthorizationException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, e: Throwable) : super(message, e)
}

class DbxServiceException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, e: Throwable) : super(message, e)
}