package pl.garner.service.dropbox

import pl.garner.service.security.SecurityUtils

object AccountContext {

    private val WS_LOGIN = ThreadLocal<String>()

    internal val login: String
        get(): String = when {
            WS_LOGIN.get() != null -> WS_LOGIN.get()
            else -> SecurityUtils.currentUserLogin()
        }

    fun setWSLogin(login: String) {
        WS_LOGIN.set(login)
    }

    fun clearWSLogin() {
        WS_LOGIN.remove()
    }
}
