package pl.garner.service.dropbox

import com.dropbox.core.v2.DbxClientV2

object DbxClientV2Holder {

    @JvmStatic
    private val INSTANCE = ThreadLocal<DbxClientV2>()

    @JvmStatic
    val instance: DbxClientV2
        get() = INSTANCE.get()

    @JvmStatic
    fun set(client: DbxClientV2) {
        INSTANCE.set(client)
    }

    @JvmStatic
    fun clear() {
        INSTANCE.remove()
    }
}
