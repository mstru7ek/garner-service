package pl.garner.service.dropbox

import com.google.common.collect.ImmutableList
import okhttp3.MediaType
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import pl.garner.service.FileMetadataDto
import java.io.IOException
import java.io.InputStream

@Service
class DbxUploadFileService(
        private val dbxStorageService: DbxStorageService,
        private val dbxImageTransformationService: DbxImageTransformationService,
        private val downloadService: DownloadService
) {

    fun saveRemote(fileUri: String): FileMetadataDto {
        val fileResource: DownloadFileResource = downloadService.download(fileUri)
        val fileInputStream = fileResource.downloadInputStream
        return transformAndStore(fileResource.mediaType, fileResource.fileName, fileInputStream)
    }

    fun saveRemote(file: MultipartFile): FileMetadataDto {
        try {
            val mediaType = MediaType.get(file.contentType!!)
            return transformAndStore(mediaType, file.originalFilename!!, file.inputStream)
        } catch (e: IOException) {
            throw DbxServiceException("Invalid input stream", e)
        }
    }

    private fun transformAndStore(mediaType: MediaType, fileName: String,
                                  fileInputStream: InputStream): FileMetadataDto {

        val isTransformable = isTransformable.invoke(mediaType)
        val inputStream: InputStream = if (isTransformable) {
            dbxImageTransformationService.transform(fileInputStream, mediaType)
        } else {
            fileInputStream
        }

        inputStream.use { input ->
            val fileMetadata = dbxStorageService.uploadFile(input, fileName)
            val sharedLink = dbxStorageService.createSharedLink(fileMetadata)
            log.info("ImageFile successfully saved in Dropbox account !!!")
            return FileMetadataDto(
                    true, null,
                    sharedLink.url,
                    sharedLink.id,
                    sharedLink.name,
                    mediaType.type())
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(DbxUploadFileService::class.java)

        private val ALLOWED_IMAGE_FILES = ImmutableList.of("jpg", "jpeg", "png", "bmp")
        private const val IMAGE_MEDIA_TYPE = "image"

        val isTransformable = { media: MediaType ->
            IMAGE_MEDIA_TYPE == media.type() && ALLOWED_IMAGE_FILES.contains(media.subtype())
        }
    }
}
