package pl.garner.service.service

import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.stereotype.Component
import org.springframework.web.context.annotation.RequestScope
import pl.garner.service.security.SecurityUtils
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest

@Component
@RequestScope
open class AccountContext(private val request: HttpServletRequest) {

    lateinit var login: String
    var namespaceId: Long = INVALID_NAMESPACE_ID

    @PostConstruct
    fun init() {
        val matcher = AntPathRequestMatcher("/api/namespace/{nsId}/**")
        val variables = matcher.extractUriTemplateVariables(request)

        val currentUserLogin = SecurityUtils.currentUserLogin()
        val nsId = variables["nsId"]

        login = currentUserLogin
        if (nsId !== null) {
            namespaceId = nsId.toLong()
        }

    }

    companion object {
        const val INVALID_NAMESPACE_ID: Long = -1
    }
}
