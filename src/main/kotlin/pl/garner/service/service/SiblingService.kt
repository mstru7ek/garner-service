package pl.garner.service.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.*
import pl.garner.service.mappper.WordsMapper
import pl.garner.service.repository.*
import pl.garner.service.web.rest.util.ResponseUtils.badRequestInvalidSiblingWordId
import pl.garner.service.web.rest.util.ResponseUtils.badRequestSiblingAlreadyExists
import java.util.*
import java.util.stream.Collectors
import java.util.stream.Stream

@Service
@Transactional(readOnly = true)
class SiblingService(private val wordRepository: WordRepository,
                     private val accountContext: AccountContext,
                     private val namespaceRepository: NamespaceRepository,
                     private val topicRepository: TopicRepository,
                     private val linkRepository: LinkRepository) {

    @Autowired
    lateinit var wordService: WordService

    fun getAllSiblings(wordId: Long): List<SiblingDto> {
        val siblings = wordRepository.findAllSiblingsByWordId(wordId)
        return WordsMapper.INSTANCE.wordToSiblingDto(siblings)
    }

    @Transactional
    fun joinWords(wordId: Long, siblingWordId: Long) {
        when {
            !wordService.findOneByIdCapsule(siblingWordId).isPresent -> throw badRequestInvalidSiblingWordId()
            wordService.isJoint(wordId, siblingWordId) -> throw badRequestSiblingAlreadyExists()
        }

        val parentWord = wordRepository.findById(wordId)
        val siblingWord = wordRepository.findById(siblingWordId)

        when {
            parentWord.map { it.topic }.isPresent -> {
                transferEntitiesToParentTopic(siblingWord, parentWord)
                joinWordsInTopic(parentWord, siblingWord, parentWord.map { it.topic })
            }
            siblingWord.map { it.topic }.isPresent -> {
                transferEntitiesToParentTopic(parentWord, siblingWord)
                joinWordsInTopic(siblingWord, parentWord, siblingWord.map { it.topic })
            }
            else -> {
                val availableTopic = topicRepository.findAllEmpty(accountContext.namespaceId)
                        .findFirst()
                        .orElseGet { this.createNewTopic() }

                joinWordsInTopic(parentWord, siblingWord, Optional.of(availableTopic))
            }
        }
    }

    private fun transferEntitiesToParentTopic(siblingWord: Optional<Word>, parentWord: Optional<Word>) {
        siblingWord.map { it.topic }
                .ifPresent { topic ->
                    val links = ArrayList(topic.links)
                    val words = ArrayList(topic.words)

                    parentWord.ifPresent { pWord ->
                        topic.links.removeAll(links)
                        topic.words.removeAll(words)

                        links.forEach { link -> link.topic = pWord.topic }
                        words.forEach { word -> word.topic = pWord.topic }

                        pWord.topic?.let {
                            it.words.addAll(words)
                            it.links.addAll(links)
                        }

                    }
                }
    }

    private fun joinWordsInTopic(parentWord: Optional<Word>,
                                 siblingWord: Optional<Word>,
                                 availableTopic: Optional<Topic>) {
        availableTopic.ifPresent { topic ->
            parentWord.ifPresent { pWord ->
                siblingWord.ifPresent { sWord ->
                    pWord.topic = topic
                    sWord.topic = topic

                    val linkParentToSibling = linkRepository.save(buildLink(pWord, topic))
                    val linkSiblingToParent = linkRepository.save(buildLink(sWord, topic))

                    linkParentToSibling.sibling = linkSiblingToParent
                    linkSiblingToParent.sibling = linkParentToSibling

                    topic.links.addAll(Arrays.asList(linkParentToSibling, linkSiblingToParent))

                    if (!topic.words.contains(pWord)) {
                        topic.words.add(pWord)
                    }
                    if (!topic.words.contains(sWord)) {
                        topic.words.add(sWord)
                    }
                }
            }
        }
    }

    private fun createNewTopic(): Topic {
        return namespaceRepository.findByCurrent(accountContext)
                .map { namespace ->
                    val createdTopic = topicRepository.save(Topic())
                    createdTopic.namespace = namespace
                    createdTopic
                }.orElseThrow { IllegalStateException("Unable to create New Topic for namespace =${accountContext.namespaceId}") }
    }

    private fun buildLink(word: Word, topic: Topic): Link {
        return Link(null, topic, word)
    }

    @Transactional
    fun deleteSiblingJoin(wordId: Long, siblingWordId: Long) {
        when {
            !wordService.isJoint(wordId, siblingWordId) -> throw badRequestInvalidSiblingWordId()
        }

        val parentWord = wordRepository.getOne(wordId)
        val siblingWord = wordRepository.getOne(siblingWordId)

        removeJoinLinks(parentWord, siblingWord)

        val parentWordTopic = parentWord.topic

        val siblingDependencyTree = findDependencyTreeForSibling(parentWordTopic!!.links.toList(), siblingWord)
        val siblingDependentWords = siblingDependencyTree.map { it.word }.toSet()

        if (!siblingDependencyTree.isEmpty()) {
            val availableTopic = topicRepository.findAllEmpty(accountContext.namespaceId)
                    .findFirst()
                    .orElseGet { this.createNewTopic() }

            transferDependencyTree(parentWordTopic, siblingDependencyTree, siblingDependentWords, availableTopic)
        }

        if (parentWordTopic.links.isEmpty()) {
            parentWord.topic = null
            parentWordTopic.words.remove(parentWord)
        }
    }

    private fun removeJoinLinks(parentWord: Word, siblingWord: Word) {
        val parentWordTopic = parentWord.topic!!
        val joinLinks = findJoinLinks(parentWord, siblingWord)

        if (joinLinks.size != 2) {
            throw IllegalStateException("Invalid link between sibling words ," +
                    " wordId = " + parentWord.id + "," +
                    " siblingWordId = " + siblingWord.id)
        }
        joinLinks.forEach { link -> link.topic = null }
        siblingWord.topic = null

        parentWordTopic.links.removeAll(joinLinks)
        parentWordTopic.words.remove(siblingWord)
        linkRepository.deleteAll(joinLinks)
    }

    private fun transferDependencyTree(parentWordTopic: Topic,
                                       siblingDependencyTree: Set<Link>,
                                       siblingDependentWords: Set<Word>,
                                       availableTopic: Topic) {

        parentWordTopic.words.removeAll(siblingDependentWords)
        parentWordTopic.links.removeAll(siblingDependencyTree)

        siblingDependencyTree.forEach { link -> link.topic = availableTopic }
        siblingDependentWords.forEach { word -> word.topic = availableTopic }

        availableTopic.links.addAll(siblingDependencyTree)
        availableTopic.words.addAll(siblingDependentWords)
    }

    private fun findJoinLinks(parentWord: Word, siblingWord: Word): List<Link> {
        return parentWord.topic!!.links
                .filter { link -> isJoiningLink(link, parentWord, siblingWord) }
    }

    private fun isJoiningLink(link: Link, parentWord: Word, siblingWord: Word): Boolean {
        val parentToSibling = link.word == parentWord && link.sibling!!.word == siblingWord
        val siblingToParent = link.word == siblingWord && link.sibling!!.word == parentWord
        return parentToSibling || siblingToParent
    }

    private fun findDependencyTreeForSibling(links: List<Link>, siblingWord: Word): Set<Link> {
        val linkComparator = Comparator.comparing<Link, Long> { it.id }
        val resultTree = TreeSet(linkComparator)

        val pendingWords = LinkedList(Arrays.asList(siblingWord))
        val consumedWords = TreeSet(Comparator.comparing<Word, Long> { it.id })

        while (!pendingWords.isEmpty()) {

            val selectedWord = pendingWords.remove()
            consumedWords.add(selectedWord)
            val selectedWordLinks = findLinksByWord(links, selectedWord)
            resultTree.addAll(selectedWordLinks)

            val nestedSiblingLinks = selectedWordLinks.stream()
                    .flatMap { link -> findLinksByWord(links, link.word).stream() }
                    .collect(Collectors.toSet())

            for (nestedLink in nestedSiblingLinks) {
                if (!consumedWords.contains(nestedLink.word)) {
                    resultTree.add(nestedLink)
                    pendingWords.add(nestedLink.word)
                }
            }
        }
        val siblingDependencyTree = resultTree.stream()
                .flatMap { link -> Stream.of(link, link.sibling!!) }
                .collect(Collectors.toSet())

        if (siblingDependencyTree.size % 2 != 0) {
            throw IllegalStateException("Inconsistent sibling dependency tree size")
        }
        return siblingDependencyTree
    }

    private fun findLinksByWord(links: List<Link>, word: Word?): Set<Link> {
        return links.stream()
                .filter { link -> link.word == word || link.sibling?.word == word }
                .collect(Collectors.toSet())
    }

    fun findSiblingSuggestions(wordId: Long, searchText: String): List<SuggestedWordDto> {
        val wordStream = wordRepository.findById(wordId)
                .map { word -> wordRepository.findAllExcludeTopic(word, word.topic, searchText, accountContext) }
                .orElseGet { Stream.empty() }

        return wordStream.map { WordsMapper.INSTANCE.wordToSuggestedWordDto(it) }.collect(Collectors.toList())
    }
}
