package pl.garner.service.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.TopicDto
import pl.garner.service.TopicWordDto
import pl.garner.service.mappper.TopicMapper
import pl.garner.service.repository.Link
import pl.garner.service.repository.Topic
import pl.garner.service.repository.TopicRepository
import pl.garner.service.repository.Word

@Service
@Transactional(readOnly = true)
class TopicService(
        private val ac: AccountContext,
        private val topicRepository: TopicRepository
) {

    fun getAllTopics(pageable: Pageable): Page<TopicDto> {
        return topicRepository.findAllNonEmpty(pageable, ac.namespaceId).map(this::buildTopicWithWords)
    }

    private fun buildTopicWithWords(topic: Topic): TopicDto {
        val wordsNames = topic.words.map { it.name }
        return TopicMapper.INSTANCE.topicToTopicDto(topic, wordsNames, wordsNames.size)
    }

    fun getAllWords(topicId: Long): List<TopicWordDto>? {
        val topic = topicRepository.findOneByIdAndNamespaceId(topicId, ac.namespaceId)
        return when {
            topic.isPresent -> this.buildTopicWordDtoList(topic.get())
            else -> null
        }
    }

    private fun buildTopicWordDtoList(topic: Topic): List<TopicWordDto> {
        return topic.words.map { word -> TopicMapper.INSTANCE.wordToTopicWordDto(word, buildSiblingsRefs(word, topic.links.toList())) }
    }

    private fun buildSiblingsRefs(word: Word, links: List<Link>): List<Long> {
        return links.filter { link -> link.word == word }.map { it.siblingWordId() }
    }
}
