package pl.garner.service.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.AddWordDto
import pl.garner.service.CoordinatesDto
import pl.garner.service.UpdateWordDto
import pl.garner.service.WordDto
import pl.garner.service.mappper.WordsMapper
import pl.garner.service.repository.NamespaceRepository
import pl.garner.service.repository.WordRepository
import pl.garner.service.web.rest.util.ResponseUtils.badRequestWordName
import java.util.*

@Service
@Transactional(readOnly = true)
class WordService(
    private val ac: AccountContext,
    private val namespaceRepository: NamespaceRepository,
    private val wordRepository: WordRepository
) {

    @Autowired
    lateinit var siblingService: SiblingService

    fun listWordsByNamespace(searchText: String?, pageable: Pageable): Page<WordDto> {
        return wordRepository.findAllJoinTopicByNamespaceId(searchText, ac, pageable)
            .map(WordsMapper.INSTANCE::wordToWordDto)
    }

    fun findOneByIdCapsule(id: Long?): Optional<WordDto> {
        return wordRepository.findById(id, ac).map { WordsMapper.INSTANCE.wordToWordDtoCapsule(it) }
    }

    fun findOneById(id: Long): Optional<WordDto> {
        return wordRepository.findById(id)
            .map { word ->
                val siblings = wordRepository.findAllSiblingsByWordId(id)
                WordsMapper.INSTANCE.wordWithSiblingsToDto(word, siblings)
            }
    }

    @Transactional
    fun createWord(addWordDto: AddWordDto): WordDto {
        if (findOneByName(addWordDto.name).isPresent) throw badRequestWordName()
        var word = WordsMapper.INSTANCE.wordDtoToWord(addWordDto)
        word = wordRepository.save(word)
        namespaceRepository.findByCurrent(ac).ifPresent { namespaceParent -> namespaceParent.words.add(word) }
        return WordsMapper.INSTANCE.wordToWordDto(word)
    }

    @Transactional
    fun updateWord(wordId: Long, updateWordDto: UpdateWordDto) {
        wordRepository.findById(wordId)
            .ifPresent { word ->
                word.translation = updateWordDto.translation
                word.transcription = updateWordDto.transcription
                word.sentence = updateWordDto.sentence
            }
    }

    fun findOneByName(name: String): Optional<WordDto> {
        return wordRepository.findOneByName(name, ac).map { WordsMapper.INSTANCE.wordToWordDto(it) }
    }

    @Transactional
    fun deactivateWord(wordId: Long) {
        siblingService.getAllSiblings(wordId)
            .forEach { siblingDto -> siblingService.deleteSiblingJoin(wordId, siblingDto.id) }

        wordRepository.findById(wordId)
            .ifPresent { word ->
                namespaceRepository.findByCurrent(ac).ifPresent { namespace -> namespace.words.remove(word) }
                word.active = false
            }
    }

    @Transactional
    fun updateCoordinates(wordId: Long, coordinatesDto: CoordinatesDto): CoordinatesDto {
        return wordRepository.findById(wordId)
            .map { word ->
                word.latitude = coordinatesDto.latitude
                word.longitude = coordinatesDto.longitude
                word
            }
            .map { WordsMapper.INSTANCE.wordToCoordinateDto(it) }
            .orElseThrow { IllegalStateException("Unable to change coordinates for word = $wordId") }
    }

    fun isJoint(wordId: Long, siblingWordId: Long): Boolean {
        return this.findOneById(wordId)
            .map { wordDto ->
                wordDto.topic?.siblings?.firstOrNull { sibling -> siblingWordId == sibling.id } != null
            }.orElse(false)
    }
}
