package pl.garner.service.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import pl.garner.service.repository.AudioFile
import pl.garner.service.repository.Word
import pl.garner.service.provider.audio.AudioUrlProvider
import java.util.stream.Collectors

@Service
class AudioProviderService(private val audioUrlProviders: List<AudioUrlProvider>)  {

    fun fetchAll(wordEntity: Word): List<AudioFile> {
        logger.trace("Start fetching urls, name= {}", wordEntity.name)

        return audioUrlProviders.parallelStream()
                .map { provider -> provider.fetchList(wordEntity.name) }
                .flatMap { it.stream() }
                .map { url -> AudioFile(wordEntity, wordEntity.name, url.toString()) }
                .sequential()
                .collect(Collectors.toList())
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AudioProviderService::class.java)
    }

}
