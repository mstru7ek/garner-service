package pl.garner.service.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.repository.AccountInfo
import pl.garner.service.repository.Namespace
import pl.garner.service.PreferencesDto
import pl.garner.service.mappper.PreferencesMapper
import pl.garner.service.repository.AccountInfoRepository
import pl.garner.service.repository.NamespaceRepository

@Service
@Transactional(readOnly = true)
class AccountService(
        private val accountInfoRepository: AccountInfoRepository,
        private val namespaceRepository: NamespaceRepository,
        private val accountContext: AccountContext
) {

    @Transactional
    fun userPreferences(): PreferencesDto {
        var accountInfo = accountInfoRepository.findCurrentBy(accountContext)
        // FIXME - po mergu przenisc do aktywacji konta, po utworzeniu konta accountInfo jest nie nullowe zawsze
        if (accountInfo == null) {
            accountInfo = accountInfoRepository.save(AccountInfo(accountContext.login))
        }
        return PreferencesMapper.INSTANCE.accountInfoToPreferencesDto(accountInfo)
    }

    @Transactional
    fun updateDefaultNamespace(namespaceId: Long): Namespace? {
        val namespace = namespaceRepository.findOneById(namespaceId, accountContext)
        val accountInfo = accountInfoRepository.findCurrentBy(accountContext)!!
        if (namespace != null) {
            accountInfo.defaultNamespace = namespace
        }
        return namespace
    }
}
