package pl.garner.service.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.repository.AccountInfo
import pl.garner.service.repository.Namespace
import pl.garner.service.NamespaceDto
import pl.garner.service.mappper.NamespaceMapper
import pl.garner.service.repository.AccountInfoRepository
import pl.garner.service.repository.NamespaceRepository
import pl.garner.service.web.rest.util.ResponseUtils.badRequestNamespaceNameExists
import java.util.*

@Service
@Transactional(readOnly = true)
class NamespaceService(private val ac: AccountContext,
                       private val namespaceRepository: NamespaceRepository,
                       private val accountInfoRepository: AccountInfoRepository) {


    fun listAll(): List<NamespaceDto> {
        return namespaceRepository.findAllByAccountInfoLogin(ac.login).map(NamespaceMapper.INSTANCE::namespaceToDto)
    }

    fun findOneById(id: Long): Optional<NamespaceDto> {
        val namespace = namespaceRepository.findOneById(id, ac)
        return Optional.ofNullable(NamespaceMapper.INSTANCE.namespaceToDto(namespace))
    }

    @Transactional
    fun createNamespace(name: String): NamespaceDto {
        if (namespaceExists(name)) {
            throw badRequestNamespaceNameExists()
        }
        var accountInfo = accountInfoRepository.findCurrentBy(ac)!!
        val namespace = namespaceRepository.save(buildNamespaceWithName(name, accountInfo))
        if (accountInfo.defaultNamespace == null) {
            accountInfo.defaultNamespace = namespace
        }
        return NamespaceMapper.INSTANCE.namespaceToDto(namespace)
    }

    @Transactional
    fun updateNamespace(namespaceId: Long, changedNamespace: NamespaceDto) {
        var namespace = namespaceRepository.findOneById(namespaceId, ac) ?: return
        namespace.name = changedNamespace.name
        namespace.active = changedNamespace.active
    }

    private fun buildNamespaceWithName(name: String, accountInfo: AccountInfo): Namespace {
        return Namespace(accountInfo = accountInfo, name = name)
    }

    private fun namespaceExists(name: String): Boolean {
        return namespaceRepository.findOneByNameAndAccountInfoLogin(name, ac.login).isPresent
    }
}
