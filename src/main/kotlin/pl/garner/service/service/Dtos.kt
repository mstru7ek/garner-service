package pl.garner.service.service

import com.fasterxml.jackson.annotation.JsonIgnore
import pl.garner.service.repository.User
import java.time.Instant
import javax.validation.constraints.*

@Target(AnnotationTarget.CLASS)
@Retention
annotation class BodyParam

/**
 * A DTO representing a password change required data - current and new password.
 */
@BodyParam
data class PasswordChangeDTO(
        val currentPassword: String,
        val newPassword: String
)

/**
 * A DTO representing a user, with his authorities.
 */

@BodyParam
data class UserDTO(
        val id: Long,

        @NotBlank
        @Pattern(regexp = LOGIN_REGEX)
        @Size(min = LOGIN_MIN_LEN, max = LOGIN_MAX_LEN)
        val login: String,

        @JsonIgnore
        @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
        val password: String,

        @Size(max = FIRST_NAME_MAX_LEN)
        val firstName: String,
        @Size(max = LAST_NAME_MAX_LEN)
        val lastName: String,

        @Email @Size(min = EMAIL_MIN_LEN, max = EMAIL_MAX_LEN)
        val email: String,

        @Size(max = IMAGE_URL_MAX_LEN)
        val imageUrl: String,

        val isActivated: Boolean,

        @Size(min = LANG_KEY_MIN_LEN, max = LANG_KEY_MAX_LEN)
        val langKey: String,

        val createdBy: String,
        val createdDate: Instant,
        val lastModifiedBy: String,
        val lastModifiedDate: Instant?,
        val authorities: Set<String>
) {

    companion object {

        const val PASSWORD_MIN_LENGTH = 4
        const val PASSWORD_MAX_LENGTH = 100
        const val LOGIN_MIN_LEN = 1
        const val LOGIN_MAX_LEN = 50
        const val FIRST_NAME_MAX_LEN = 50
        const val LAST_NAME_MAX_LEN = 50
        const val EMAIL_MIN_LEN = 5
        const val EMAIL_MAX_LEN = 254
        const val IMAGE_URL_MAX_LEN = 256
        const val LANG_KEY_MIN_LEN = 2
        const val LANG_KEY_MAX_LEN = 6

        const val LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$"

        fun of(user: User): UserDTO {
            return UserDTO(
                    id = user.id ?: 0,
                    login = user.login,
                    firstName = user.firstName,
                    lastName = user.lastName,
                    email = user.email,
                    isActivated = user.activated,
                    imageUrl = user.imageUrl ?: "",
                    langKey = user.langKey,
                    createdDate = user.createdDate,
                    authorities = user.authorities.map { it.name }.toSet(),
                    password = "",
                    createdBy = "",
                    lastModifiedBy = "",
                    lastModifiedDate = null
            )
        }
    }
}

/**
 * View Model object for storing a user's credentials.
 */
@BodyParam
data class LoginVM(

        @NotNull
        @Size(min = USERNAME_MIN_LEN, max = USERNAME_MAX_LEN)
        val username: String,

        @NotNull
        @Size(min = UserDTO.PASSWORD_MIN_LENGTH, max = UserDTO.PASSWORD_MAX_LENGTH)
        val password: String,
        val rememberMe: Boolean = true
) {

    companion object {
        const val USERNAME_MIN_LEN = 1
        const val USERNAME_MAX_LEN = 50
    }
}

/**
 * View Model object for storing the user's key and password.
 */

@BodyParam
data class KeyAndPasswordVM(
        val key: String = "",
        val newPassword: String = ""
)
