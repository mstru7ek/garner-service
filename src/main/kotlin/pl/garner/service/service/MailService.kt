package pl.garner.service.service

import io.github.jhipster.config.JHipsterProperties
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine
import pl.garner.service.repository.User
import java.nio.charset.StandardCharsets
import java.util.*

/**
 * Service for sending emails.
 *
 *
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
class MailService(
        private val jHipsterProperties: JHipsterProperties,
        private val javaMailSender: JavaMailSender,
        private val messageSource: MessageSource,
        private val templateEngine: SpringTemplateEngine
) {

    @Async
    fun sendActivationEmail(user: User) {
        log.debug("Sending activation email to '{}'", user.email)
        sendEmailFromTemplate(user, ACTIVATION_EMAIL_TEMPLATE, ACTIVATION_EMAIL_TITLE)
    }

    @Async
    fun sendPasswordResetMail(user: User) {
        log.debug("Sending password reset email to '{}'", user.email)
        sendEmailFromTemplate(user, PASSROWD_RESET_EMAIL_TEMPLATE, PASSWORD_RESET_EMAIL_TITLE)
    }

    @Async
    private fun sendEmail(to: String, subject: String, content: String, isMultipart: Boolean, isHtml: Boolean) {

        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
                isMultipart, isHtml, to, subject, content)

        // Prepare message using a Spring helper
        val mimeMessage = javaMailSender.createMimeMessage()
        try {
            val message = MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name())
            message.setTo(to)
            message.setFrom(jHipsterProperties.mail.from)
            message.setSubject(subject)
            message.setText(content, isHtml)
            javaMailSender.send(mimeMessage)
            log.debug("Sent email to User '{}'", to)
        } catch (e: Exception) {
            log.warn("Email could not be sent to user '{}'", to, e)
        }
    }

    @Async
    private fun sendEmailFromTemplate(user: User, templateName: String, titleKey: String) {
        val locale = Locale.forLanguageTag(user.langKey)
        val context = Context(locale)
        context.setVariable(USER, user)
        context.setVariable(BASE_URL, jHipsterProperties.mail.baseUrl)
        val content = templateEngine.process(templateName, context)
        val subject = messageSource.getMessage(titleKey, null, locale)
        sendEmail(user.email, subject, content, false, isHtml = true)
    }

    companion object {
        private val log = LoggerFactory.getLogger(MailService::class.java)

        private const val USER = "user"
        private const val BASE_URL = "baseUrl"

        const val PASSROWD_RESET_EMAIL_TEMPLATE = "mail/passwordResetEmail"
        const val PASSWORD_RESET_EMAIL_TITLE = "email.reset.title"
        const val ACTIVATION_EMAIL_TEMPLATE = "mail/activationEmail"
        const val ACTIVATION_EMAIL_TITLE = "email.activation.title"
    }
}
