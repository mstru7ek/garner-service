package pl.garner.service.service

import org.slf4j.LoggerFactory
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.repository.Authority
import pl.garner.service.repository.User
import pl.garner.service.repository.UserRepository
import pl.garner.service.security.AuthoritiesConstants
import pl.garner.service.security.SecurityUtils
import pl.garner.service.service.util.RandomUtil
import pl.garner.service.web.rest.errors.*
import java.time.Instant

/**
 * Service class for managing users.
 */
@Service
@Transactional
class UserService(
        private val userRepository: UserRepository,
        private val passwordEncoder: PasswordEncoder
) {

    @Transactional(readOnly = true)
    fun userWithAuthorities(): UserDTO {
        val currentUser = SecurityUtils.currentUserLogin()
        return userRepository.findOneWithAuthoritiesByLogin(currentUser).map { UserDTO.of(it) }.get()
    }

    fun activateRegistration(key: String): User {
        log.debug("Activating user for activation key {}", key)
        return userRepository.findOneByActivationKey(key)
                .map { user ->
                    user.activated = true
                    user.authorities = hashSetOf(Authority.of(AuthoritiesConstants.USER))
                    log.debug("Activated user: {}", user)
                    user
                }.orElseThrow { InternalServerErrorException("No user was found for this activation key") }
    }

    fun completePasswordReset(newPassword: String, key: String): User {
        log.debug("Reset user password for reset key {}", key)
        return userRepository.findOneByResetKey(key)
                .filter { user ->
                    user.resetDate.isAfter(Instant.now().minusSeconds(RESET_KEY_EXPIRATION_MS.toLong()))
                }.map { user ->
                    user.password = passwordEncoder.encode(newPassword)
                    user.resetKey = EMPTY
                    user.resetDate = Instant.EPOCH
                    user
                }.orElseThrow { InternalServerErrorException("No user was found for this reset key") }
    }

    fun requestPasswordReset(mail: String): User {
        return userRepository.findOneByEmailIgnoreCase(mail)
                .filter { it.activated }
                .map { user ->
                    user.resetKey = RandomUtil.generateResetKey()
                    user.resetDate = Instant.now()
                    user
                }.orElseThrow { EmailNotFoundException() }
    }

    fun registerUser(userDTO: UserDTO, password: String): User {
        if (exists(userDTO.login)) {
            throw LoginAlreadyUsedException()
        }

        val encryptedPassword = passwordEncoder.encode(password)
        val newUser = User(
                id = null,
                login = userDTO.login.toLowerCase(),
                password = encryptedPassword,
                firstName = userDTO.firstName,
                lastName = userDTO.lastName,
                email = userDTO.email.toLowerCase(),
                imageUrl = userDTO.imageUrl,
                langKey = DEFAULT_LANGUAGE,
                activated = false,
                activationKey = RandomUtil.generateActivationKey(),
                resetKey = EMPTY,
                authorities = hashSetOf(Authority.of(AuthoritiesConstants.USER))
        )

        userRepository.save(newUser)
        log.debug("Created Information for User: {}", newUser)
        return newUser
    }

    private fun removeNonActivatedUser(existingUser: User): Boolean {
        if (existingUser.activated) {
            return false
        }
        userRepository.delete(existingUser)
        userRepository.flush()
        return true
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     * @param firstName first name of user
     * @param lastName  last name of user
     * @param email     email id of user
     * @param langKey   language key
     * @param imageUrl  image URL of user
     */
    fun updateUser(firstName: String, lastName: String, email: String, langKey: String, imageUrl: String) {
        val currentUser = SecurityUtils.currentUserLogin()
        userRepository.findOneByLogin(currentUser)
                .ifPresent { user ->
                    user.firstName = firstName
                    user.lastName = lastName
                    user.email = email.toLowerCase()
                    user.langKey = langKey
                    user.imageUrl = imageUrl
                    log.debug("Changed Information for User: {}", user)
                }
    }

    fun changePassword(currentClearTextPassword: String, newPassword: String) {
        val currentUser = SecurityUtils.currentUserLogin()
                userRepository.findOneByLogin(currentUser)
                .ifPresent { user ->
                    if (!passwordEncoder.matches(currentClearTextPassword, user.password)) {
                        throw InvalidPasswordException()
                    }
                    val encryptedPassword = passwordEncoder.encode(newPassword)
                    user.password = encryptedPassword
                    log.debug("Changed password for User: {}", user)
                }
    }

    @Transactional(readOnly = true)
    fun exists(login: String): Boolean {
        return userRepository.findOneByLogin(login.toLowerCase()).isPresent
    }

    fun saveAccount(userDTO: UserDTO) {
        val currentUser = SecurityUtils.currentUserLogin()
        val existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.email)
        if (existingUser.isPresent && !existingUser.get().login.equals(currentUser, ignoreCase = true)) {
            throw EmailAlreadyUsedException()
        }

        val user = userRepository.findOneByLogin(currentUser)
        if (!user.isPresent) {
            throw InternalServerErrorException("User could not be found")
        }
        updateUser(userDTO.firstName, userDTO.lastName, userDTO.email, userDTO.langKey, userDTO.imageUrl)
    }

    companion object {
        private val log = LoggerFactory.getLogger(UserService::class.java)
        const val RESET_KEY_EXPIRATION_MS = 86400
        const val DEFAULT_LANGUAGE = "en"
        const val EMPTY = ""
    }
}
