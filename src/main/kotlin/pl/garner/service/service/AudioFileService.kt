package pl.garner.service.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.garner.service.AudioFileDto
import pl.garner.service.CreateAudioFileDto
import pl.garner.service.mappper.MediaMapper
import pl.garner.service.repository.AudioFileRepository
import pl.garner.service.repository.WordRepository
import pl.garner.service.web.rest.util.ResponseUtils

@Service
class AudioFileService(
        private val audioFileRepository: AudioFileRepository,
        private val wordRepository: WordRepository,
        private val audioProviderService: AudioProviderService
)  {

    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable, wordId: Long): Page<AudioFileDto> {
        return audioFileRepository.findAllByWordId(pageable, wordId).map(MediaMapper.INSTANCE::mediaToMediaDto)
    }

    @Transactional
    fun addFile(wordId: Long, createAudioFileDto: CreateAudioFileDto): AudioFileDto {
        val word = wordRepository.findOneByWordId(wordId) ?: throw ResponseUtils.notFoundWordEntity()
        var audioFile = MediaMapper.INSTANCE.addMediaDtoToMedia(createAudioFileDto).also { it.word = word }
        audioFile = audioFileRepository.save(audioFile)
        return MediaMapper.INSTANCE.mediaToMediaDto(audioFile)
    }

    @Transactional
    fun reloadBy(wordId: Long): Page<AudioFileDto> {
        val word = wordRepository.findOneByWordId(wordId) ?: throw ResponseUtils.notFoundWordEntity()
        val newAudioFileList = audioProviderService.fetchAll(word)
        audioFileRepository.deleteAllByWord(word)
        audioFileRepository.saveAll(newAudioFileList)
        return PageImpl(MediaMapper.INSTANCE.mediaToMediaDto(newAudioFileList))
    }
}
