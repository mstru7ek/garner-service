package pl.garner.service

import java.time.LocalDateTime
import javax.validation.Valid
import javax.validation.constraints.NotNull

@Target(AnnotationTarget.CLASS)
@Retention
annotation class NoArgConstructor

@NoArgConstructor
class AddSiblingDto(val wordId: Long)

@NoArgConstructor
class AddWordDto(
        @NotNull val name: String,
        @NotNull val translation: String,
        @NotNull val sentence: String,
        @NotNull val transcription: String,
        @NotNull val latitude: Double,
        @NotNull val longitude: Double
)

@NoArgConstructor
class AudioFileDto(var id: Long, var name: String, var url: String)

@NoArgConstructor
class CoordinatesDto(var latitude: Double = 0.0, var longitude: Double = 0.0)

@NoArgConstructor
class CreateAudioFileDto(@NotNull val name: String, @NotNull val url: String)

@NoArgConstructor
class NamespaceDto(
        var id: Long,
        var name: String,
        var active: Boolean
)

@NoArgConstructor
class PreferencesDto(
        var id: Long,
        var login: String,
        var namespace: NamespaceDto,
        var dbxCsrfToken: String,
        var dbxUserId: String,
        var dbxAccountId: String,
        var dbxAccessToken: String
)

@NoArgConstructor
class SiblingDto(
        var id: Long,
        var active: Boolean,
        var name: String,
        var translation: String
)

@NoArgConstructor
class SuggestedWordDto(
        var id: Long,
        var name: String,
        var translation: String
)

@NoArgConstructor
class TopicDto(
        var id: Long,
        var size: Int,
        var wordsNames: List<String>
)

@NoArgConstructor
class TopicWordDto(
        var id: Long,
        var active: Boolean,
        var createdDate: LocalDateTime,
        var name: String,
        var translation: String,
        var sentence: String?,
        var transcription: String?,
        var latitude: Double?,
        var longitude: Double?,
        var topic: TopicDto
) {

    @NoArgConstructor
    class TopicDto(var id: Long, var siblingsRefs: List<Long>)
}

@NoArgConstructor
class UpdatePreferencesDto(@NotNull @Valid var namespace: NamespaceUpdateDto) {

    @NoArgConstructor
    class NamespaceUpdateDto(@NotNull var id: Long)
}

@NoArgConstructor
class UpdateWordDto(
        @NotNull var translation: String,
        @NotNull var sentence: String,
        @NotNull var transcription: String
)

@NoArgConstructor
class WordDto(
        var id: Long,
        var active: Boolean,
        var createdDate: LocalDateTime,
        var name: String,
        var translation: String,
        var sentence: String?,
        var transcription: String?,
        var latitude: Double?,
        var longitude: Double?,
        var topic: TopicDto?
) {

    @NoArgConstructor
    class TopicDto(
            var id: Long,
            var size: Int,
            var siblings: List<SiblingDto>,
            var namespaceId: Long
    )
}

@NoArgConstructor
data class MigrationId(var key: String)

@NoArgConstructor
data class DeleteAllFiles(var namespaceId: Long, var wordId: Long)

data class DbxAccessTokenDto(
        val isValid: Boolean
)

data class FileMetadataDto(
        val isValid: Boolean,
        val errorMsg: String?,
        val url: String,
        val id: String,
        val name: String,
        val type: String
) {
    companion object {
        fun valid(url: String, id: String, name: String, type: String) = FileMetadataDto(true, null, url, id, name, type)
    }
}

data class DbxAuthorizeUri(
        val redirectUri: String
)

data class DbxAuthenticationDto(
        val isSuccess: Boolean
) {
    companion object {
        fun completed(): DbxAuthenticationDto {
            return DbxAuthenticationDto(true)
        }
    }
}