package pl.garner.service.provider.audio

import org.apache.commons.lang3.StringUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*
import kotlin.collections.ArrayList
import kotlin.streams.toList

@Component
class VocabularyAudioUrlProvider : AudioUrlProvider {

    override val type: ProviderType = ProviderType.VOCABULARY

    override fun fetchList(queryWord: String): List<URI> {

        val queryParameter = StringUtils.replace(queryWord, StringUtils.SPACE, WORD_SEPARATOR)
        val webAddress = String.format(URL_FORMAT, queryParameter)
        val url = URI.create(webAddress)

        val content = HttpClientUtils.downloadContent(url)
        val document = Jsoup.parse(content) ?: return Arrays.asList()

        val audioUrlList = ArrayList<URI>()
        for (titleElement in document.select(TITLE_SELECTOR)) {
            if (isTitleEqualToQuery(titleElement, queryWord)) {
                for (dataAudioElement in titleElement.getElementsByAttribute(DATA_AUDIO_ATTRIBUTE) ) {
                    val attribute = dataAudioElement.attr(DATA_AUDIO_ATTRIBUTE)
                    val audioUrlAddress = String.format(AUDIO_URL_FORMAT, attribute)
                    val audioUrl = URI.create(audioUrlAddress)
                    audioUrlList.add(audioUrl)
                }
            }
        }
        return audioUrlList.parallelStream()
                .filter { HttpClientUtils.isValidAudioURI(it) }
                .toList()
    }

    companion object {

        private const val URL_FORMAT = "https://www.vocabulary.com/dictionary/%s"
        private const val AUDIO_URL_FORMAT = "https://audio.vocab.com/1.0/us/%s.mp3"
        private const val WORD_SEPARATOR = "%20"
        private const val TITLE_SELECTOR = "#pageContent h1.dynamictext"
        private const val DATA_AUDIO_ATTRIBUTE = "data-audio"

        private fun isTitleEqualToQuery(element: Element, queryParameter: String): Boolean {
            return StringUtils.trimToEmpty(element.text()) == queryParameter
        }
    }
}
