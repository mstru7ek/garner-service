package pl.garner.service.provider.audio

import org.apache.http.Header
import org.apache.http.HeaderElement
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpHead
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.http.util.EntityUtils
import org.slf4j.LoggerFactory
import java.io.IOException
import java.net.URI
import java.util.*


object HttpClientUtils {

    private val httpClient: CloseableHttpClient = HttpClients.custom()
            .setConnectionManager(PoolingHttpClientConnectionManager())
            .setRetryHandler(DefaultHttpRequestRetryHandler(3, true))
            .build()

    internal fun isValidAudioURI(audioUri: URI): Boolean {
        val httpHead = HttpHead(audioUri)
        try {
            val httpResponse = httpClient.execute(httpHead)
            httpResponse.use { response ->
                logger.trace("Connection url: {}", audioUri)
                logger.trace("Connection statusLine: {}", response.statusLine)
                EntityUtils.consume(response.entity)

                if (HTTP_OK == response.statusLine.statusCode) {

                    val isAudioFileContentType = Arrays.stream(response.allHeaders)
                            .filter { isContentTypeHeader(it) }
                            .flatMap { header -> Arrays.stream(header.elements) }
                            .filter { isAudioFileContentType(it) }
                            .findAny()

                    return isAudioFileContentType.isPresent
                }
                return false
            }
        } catch (e: ClientProtocolException) {
            logger.error("Client protocol error", e)
            throw HttpConnectionException(e)
        } catch (e: IOException) {
            logger.error("Unable to connect to external resource", e)
            throw HttpConnectionException(e)
        }

    }

    internal fun downloadContent(requestUri: URI): String? {
        val httpGet = HttpGet(requestUri)
        try {
            httpClient.execute(httpGet).use { response ->
                logger.trace("Connection url: {}", requestUri)
                logger.trace("Connection statusLine: {}", response.statusLine)
                if (HTTP_OK == response.statusLine.statusCode) {
                    return EntityUtils.toString(response.entity, "UTF-8")
                }
            }
        } catch (e: ClientProtocolException) {
            logger.error("Client protocol error", e)
            throw HttpConnectionException(e)
        } catch (e: IOException) {
            logger.error("Unable to connect to external resource", e)
            throw HttpConnectionException(e)
        }

        return null
    }

    private fun isAudioFileContentType(headerElement: HeaderElement): Boolean {
        return AUDIO_MPEG_MIME_TYPE == headerElement.name || BINARY_OCTET_STREAM_MIME_TYPE == headerElement.name
    }

    private fun isContentTypeHeader(header: Header): Boolean {
        return header.name.equals(HEADER_CONTENT_TYPE, ignoreCase = true)
    }

    private val logger = LoggerFactory.getLogger(HttpClientUtils::class.java)

    private const val HEADER_CONTENT_TYPE = "Content-type"
    private const val HTTP_OK = 200
    private const val AUDIO_MPEG_MIME_TYPE = "audio/mpeg"
    private const val BINARY_OCTET_STREAM_MIME_TYPE = "binary/octet-stream"
}

class HttpConnectionException(cause: Throwable) : RuntimeException(cause)