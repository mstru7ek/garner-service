package pl.garner.service.provider.audio

import com.google.common.collect.ImmutableList
import org.apache.commons.lang3.StringUtils
import org.jsoup.Jsoup
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*
import kotlin.streams.toList

@Component
class OxfordAudioUrlProvider : AudioUrlProvider {

    override val type: ProviderType = ProviderType.OXFORD

    override fun fetchList(queryWord: String): List<URI> {

        val queryParameter = StringUtils.replace(queryWord, StringUtils.SPACE, WORD_SEPARATOR)
        val webAddress = String.format(URL_FORMAT, queryParameter)
        val url = URI.create(webAddress)

        val content = HttpClientUtils.downloadContent(url)
        val document = Jsoup.parse(content) ?: return ImmutableList.of()

        val urlAttributes = HashSet<String>()
        for (element in document.select(DICTIONARY_WORD_ENTITY_SELECTOR)) {
            for (urlAttributeElement in element.getElementsByAttribute(DATA_AUDIO_URL_ATTRIBUTE)) {
                urlAttributes.add(urlAttributeElement.attr(DATA_AUDIO_URL_ATTRIBUTE))
            }
        }

        val audioUrlList = ArrayList<URI>()
        for (urlAttribute in urlAttributes) {
            if (isQueryWordInPathUrl(queryWord, urlAttribute)) {
                val audioUrl = URI.create(urlAttribute)
                audioUrlList.add(audioUrl)
            }
        }
        return audioUrlList.parallelStream()
                .filter { HttpClientUtils.isValidAudioURI(it) }
                .toList()
    }

    companion object {

        private const val URL_FORMAT = "https://en.oxforddictionaries.com/definition/%s"
        private const val WORD_SEPARATOR = "_"
        private const val DICTIONARY_WORD_ENTITY_SELECTOR = "div.entryWrapper audio"
        private const val DATA_AUDIO_URL_ATTRIBUTE = "src"

        private fun isQueryWordInPathUrl(wordQuery: String, path: String): Boolean {
            return StringUtils.split(wordQuery).all { path.contains(it) }
        }
    }

}
