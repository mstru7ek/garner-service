package pl.garner.service.provider.audio

import java.net.URI

interface AudioUrlProvider {

    /**
     * Audio Provider
     * @return
     */
    val type: ProviderType

    /**
     * Fetch audio url list
     * @param queryWord query word
     * @return url list or null if there is problem with connection
     */
    fun fetchList(queryWord: String): List<URI>
}
