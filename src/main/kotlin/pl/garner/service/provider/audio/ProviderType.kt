package pl.garner.service.provider.audio

enum class ProviderType {
    DIKI,
    VOCABULARY,
    OXFORD,
    CAMBRIDGE
}
