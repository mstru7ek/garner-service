package pl.garner.service.provider.audio

import org.apache.commons.lang3.StringUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*
import java.util.function.Function
import java.util.stream.Collectors

@Component
class CambridgeAudioUrlProvider : AudioUrlProvider {

    override val type: ProviderType = ProviderType.CAMBRIDGE

    override fun fetchList(queryWord: String): List<URI> {

        val queryParameter = StringUtils.replace(queryWord, StringUtils.SPACE, WORD_SEPARATOR)
        val webAddress = String.format(URL_FORMAT, queryParameter)
        val url = URI.create(webAddress)

        val content = HttpClientUtils.downloadContent(url)
        val document = Jsoup.parse(content) ?: return Arrays.asList()

        return document.select(SPAN_EL_WITH_MP3_ATTR).stream().map(extractAudioUrlAttr).distinct().map(asURL)
                .parallel()
                .filter(onlyValidAudioContent).collect(Collectors.toList())
    }

    companion object {

        private const val BASE_URL = "https://dictionary.cambridge.org"
        private const val URL_FORMAT = "https://dictionary.cambridge.org/dictionary/english/%s"
        private const val WORD_SEPARATOR = "-"
        private const val SPAN_EL_WITH_MP3_ATTR = "source[type='audio/mpeg']"
        private const val DATA_AUDIO_URL_ATTRIBUTE = "src"

        private val extractAudioUrlAttr = { e: Element -> e.attr(DATA_AUDIO_URL_ATTRIBUTE) }
        private val onlyValidAudioContent = { uri: URI -> HttpClientUtils.isValidAudioURI(uri) }
        private val asURL = { webAddress: String -> URI.create(BASE_URL + webAddress) }
    }
}
