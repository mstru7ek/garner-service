package pl.garner.service.web.rest

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.garner.service.security.jwt.JWTFilter
import pl.garner.service.security.jwt.TokenProvider
import pl.garner.service.service.LoginVM
import javax.validation.Valid

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
class UserJWTController(
        private val tokenProvider: TokenProvider,
        private val authenticationManager: AuthenticationManager
) {

    @PostMapping("/authenticate")
    fun authorize(@Valid @RequestBody loginVM: LoginVM): ResponseEntity<JWTToken> {

        val authentication = this.authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(loginVM.username, loginVM.password)
        )
        SecurityContextHolder.getContext().authentication = authentication
        val jwt = tokenProvider.createToken(authentication, loginVM.rememberMe)
        return ResponseEntity(JWTToken(jwt), jwtHeader(jwt), HttpStatus.OK)
    }

    companion object {

        fun jwtHeader(jwtToken: String): HttpHeaders {
            val headers = HttpHeaders()
            headers.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer $jwtToken")
            return headers
        }
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    class JWTToken(@get:JsonProperty("id_token") val idToken: String)
}
