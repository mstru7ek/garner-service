package pl.garner.service.web.rest

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.garner.service.NamespaceDto
import pl.garner.service.service.NamespaceService
import java.net.URI
import javax.validation.Valid

@RestController
@RequestMapping("/api/namespace")
class NamespaceResource(
        private val namespaceService: NamespaceService
) {

    @GetMapping("")
    fun listAllNamespaces(): ResponseEntity<*> {
        val namespaces = namespaceService.listAll()
        return ResponseEntity.ok(namespaces)
    }

    @PostMapping("")
    fun createNewNamespace(@Valid @RequestBody newNamespace: NamespaceDto): ResponseEntity<*> {
        val namespaceDto = namespaceService.createNamespace(newNamespace.name)
        return ResponseEntity.created(URI.create("/api/namespace/${namespaceDto.id}")).body(namespaceDto)
    }

    @GetMapping("/{nsId}")
    fun namespaceById(@PathVariable("nsId") namespaceId: Long): ResponseEntity<*> {
        return namespaceService.findOneById(namespaceId)
                .map { namespace -> ResponseEntity(namespace, HttpStatus.OK) }
                .orElseGet { ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR) }
    }

    @PutMapping("/{nsId}")
    fun updateNamespace(@PathVariable("nsId") namespaceId: Long,
                        @Valid @RequestBody changedNamespace: NamespaceDto): ResponseEntity<*> {
        namespaceService.updateNamespace(namespaceId, changedNamespace)
        return ResponseEntity.ok().build<Any>()
    }
}
