package pl.garner.service.web.rest

import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.garner.service.CreateAudioFileDto
import pl.garner.service.service.AccountContext
import pl.garner.service.service.AudioFileService
import java.net.URI
import javax.validation.Valid

@RestController
@RequestMapping("/api/namespace/{nsId}/word/{wId}")
class AudioFileResource(
        private val ac: AccountContext,
        private val audioFileService: AudioFileService
) {

    @GetMapping("/audio")
    fun listAllAudioFiles(@PageableDefault(size = 50) pageable: Pageable,
                          @PathVariable("wId") wordId: Long): ResponseEntity<*> {
        val page = audioFileService.findAll(pageable, wordId)
        return ResponseEntity.ok(page)
    }

    @PostMapping("/audio")
    fun addAudioFile(@PathVariable("wId") wordId: Long,
                     @Valid @RequestBody createAudioFileDto: CreateAudioFileDto): ResponseEntity<*> {
        val audioFileDto = audioFileService.addFile(wordId, createAudioFileDto)
        val mediaLocation = URI.create("/api/namespace/${ac.namespaceId}/word/$wordId/audio")
        return ResponseEntity.created(mediaLocation).body(audioFileDto)
    }

    @PutMapping("/audio/reload")
    fun updateAudioFileList(@PathVariable("wId") wordId: Long): ResponseEntity<*> {
        val wordMediaContent = audioFileService.reloadBy(wordId)
        return ResponseEntity.ok(wordMediaContent)
    }
}
