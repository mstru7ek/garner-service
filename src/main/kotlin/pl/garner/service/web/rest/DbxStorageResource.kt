package pl.garner.service.web.rest

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import pl.garner.service.DeleteAllFiles
import pl.garner.service.dropbox.DbxUploadFileService
import pl.garner.service.dropbox.FileService
import pl.garner.service.dropbox.transfer.MigrationService
import pl.garner.service.repository.FileMetadata
import java.net.URI
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

@RestController
class DbxStorageResource(
        private val fileService: FileService,
        private val dbxUploadFileService: DbxUploadFileService,
        private val migrationService: MigrationService
) {

    @PostMapping("/api/dbx/storage/upload")
    fun handleUpload(@RequestParam namespaceId: Long, @RequestParam wordId: Long, @RequestParam wordName: String,
                     @RequestParam file: MultipartFile): ResponseEntity<*> {
        val fileMetadataDto = dbxUploadFileService.saveRemote(file)
        val fileMetadataURI = fileService.saveFileMetadata(namespaceId, wordId, wordName, fileMetadataDto)

        val httpHeaders = HttpHeaders()
        httpHeaders.location = URI.create(fileMetadataURI)
        return ResponseEntity.status(CREATED).headers(httpHeaders).build<Any>()
    }

    @PostMapping("/api/dbx/storage/upload/uri")
    fun handleUpload(@RequestParam namespaceId: Long, @RequestParam wordId: Long, @RequestParam wordName: String,
                     @RequestParam fileUri: String): ResponseEntity<*> {
        val fileMetadataDto = dbxUploadFileService.saveRemote(fileUri)
        val fileMetadataURI = fileService.saveFileMetadata(namespaceId, wordId, wordName, fileMetadataDto)

        val httpHeaders = HttpHeaders()
        httpHeaders.location = URI.create(fileMetadataURI)
        return ResponseEntity.status(CREATED).headers(httpHeaders).build<Any>()
    }

    @DeleteMapping("/api/dbx/storage/files")
    fun findAllFileMetadata(@RequestParam namespaceId: Long, @RequestParam url: String): ResponseEntity<*> {
        fileService.deleteFile(namespaceId, url)
        return ResponseEntity.status(NO_CONTENT).build<Any>()
    }

    @GetMapping("/api/dbx/storage/files")
    fun findAllFileMetadata(@RequestParam namespaceId: Long, @RequestParam wordId: Long): List<FileMetadata> {
        return fileService.findAll(namespaceId, wordId)
    }

    @PutMapping("/api/dbx/storage/files")
    fun deleteWordFiles(@RequestBody request: DeleteAllFiles): ResponseEntity<*> {
        fileService.deleteAllFiles(request.namespaceId, request.wordId)
        return ResponseEntity.status(NO_CONTENT).build<Any>()
    }

    @GetMapping("/api/dbx/storage/files/lookBack")
    fun lookBack(@RequestParam namespaceId: Long, pageable: Pageable): Page<FileMetadata> {
        return fileService.lookBack(namespaceId, pageable)
    }

    @PostMapping("/api/dbx/storage/reload")
    fun reloadStorage(httpServletRequest: HttpServletRequest): ResponseEntity<*> {
        val migrationId = httpServletRequest.session.id
        this.migrationService.startMigration(migrationId)
        return ResponseEntity.ok().build<Any>()
    }

    @GetMapping("/api/dbx/storage/reload")
    fun reloadStorageStatus(session: HttpSession): SseEmitter {
        return migrationService.buildStatusEmitter(session.id)
    }
}