package pl.garner.service.web.rest.util

import org.zalando.problem.Problem
import org.zalando.problem.Status.BAD_REQUEST
import org.zalando.problem.ThrowableProblem
import pl.garner.service.web.rest.errors.ExceptionTranslator.Companion.CONSTRAINT_VIOLATION_TYPE
import pl.garner.service.web.rest.errors.ExceptionTranslator.Companion.ENTITY_NOT_FOUND_TYPE


object ResponseUtils {

    fun notFoundWordEntity(): ThrowableProblem {
        return Problem.builder().withType(ENTITY_NOT_FOUND_TYPE).withTitle(WORD)
                .withStatus(BAD_REQUEST)
                .withDetail("Invalid word id").build()
    }

    fun badRequestWordName(): ThrowableProblem {
        return Problem.builder().withType(CONSTRAINT_VIOLATION_TYPE).withTitle(WORD)
                .withStatus(BAD_REQUEST)
                .withDetail("Word name already in current namespace").build()
    }

    fun notFoundNamespaceEntity(): ThrowableProblem {
        return Problem.builder().withType(CONSTRAINT_VIOLATION_TYPE).withTitle(NAMESPACE)
                .withStatus(BAD_REQUEST)
                .withDetail("Invalid namespace id").build()
    }

    fun badRequestNamespaceNameExists(): ThrowableProblem {
        return Problem.builder().withType(CONSTRAINT_VIOLATION_TYPE).withTitle(NAMESPACE)
                .withStatus(BAD_REQUEST)
                .withDetail("Namespace name already in use").build()
    }

    fun badRequestSiblingAlreadyExists(): ThrowableProblem {
        return Problem.builder().withType(CONSTRAINT_VIOLATION_TYPE).withTitle(SIBLING)
                .withStatus(BAD_REQUEST)
                .withDetail("Sibling already in use").build()
    }

    fun badRequestInvalidSiblingWordId(): ThrowableProblem {
        return Problem.builder().withType(ENTITY_NOT_FOUND_TYPE).withTitle(SIBLING)
                .withStatus(BAD_REQUEST)
                .withDetail("Sibling word id doesn't exist in namespace").build()
    }

    fun notFoundTopicEntity(): ThrowableProblem {
        return Problem.builder().withType(ENTITY_NOT_FOUND_TYPE).withTitle(TOPIC)
                .withStatus(BAD_REQUEST)
                .withDetail("Invalid topic id").build()
    }

    const val WORD = "WORD"
    const val NAMESPACE = "NAMESPACE"
    const val TOPIC = "TOPIC"
    const val LOGIN = "LOGIN"
    const val SIBLING = "SIBLING"
    const val EMAIL = "EMAIL"
    const val PASSWORD = "PASSWORD"
    const val AUTHENTICATION = "AUTHENTICATION"
    const val IMAGE = "IMAGE"
}


