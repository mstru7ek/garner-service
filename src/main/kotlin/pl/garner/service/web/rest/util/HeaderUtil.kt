package pl.garner.service.web.rest.util

import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders

/**
 * Utility class for HTTP headers creation.
 */
object HeaderUtil {

    private val log = LoggerFactory.getLogger(HeaderUtil::class.java)

    private const val APPLICATION_NAME = "garner-dbx"

    fun createFailureAlert(entityName: String, errorKey: String, defaultMessage: String): HttpHeaders {
        log.error("Entity processing failed, {}", defaultMessage)
        val headers = HttpHeaders()
        headers.add("X-$APPLICATION_NAME-error", "error.$errorKey")
        headers.add("X-$APPLICATION_NAME-params", entityName)
        return headers
    }
}
