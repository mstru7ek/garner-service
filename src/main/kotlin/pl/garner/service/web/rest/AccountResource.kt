package pl.garner.service.web.rest

import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pl.garner.service.repository.UserRepository
import pl.garner.service.web.rest.errors.EmailAlreadyUsedException
import pl.garner.service.web.rest.errors.EmailNotFoundException
import pl.garner.service.web.rest.errors.InvalidPasswordException
import pl.garner.service.web.rest.errors.LoginAlreadyUsedException
import pl.garner.service.service.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
class AccountResource(
        private val userRepository: UserRepository,
        private val userService: UserService,
        private val mailService: MailService
) {

    /**
     * GET  /account : get the current user.
     * @return the current user
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be returned
     */
    @GetMapping("/account")
    fun account(): UserDTO {
        return userService.userWithAuthorities()
    }

    /**
     * POST  /register : register the user.
     * @param managedUserVM the managed user View Model
     * @throws InvalidPasswordException  400 (Bad Request) if the password is incorrect
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is already used
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    fun registerAccount(@Valid @RequestBody managedUserVM: UserDTO) {
        checkPasswordLength(managedUserVM.password)
        val user = userService.registerUser(managedUserVM, managedUserVM.password)
        mailService.sendActivationEmail(user)
    }

    /**
     * GET  /activate : activate the registered user.
     * @param key the activation key
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be activated
     */
    @GetMapping("/activate")
    fun activateAccount(@RequestParam(value = "key") key: String) {
        userService.activateRegistration(key)
    }

    /**
     * GET  /authenticate : check if the user is authenticated, and return its login.
     * @param request the HTTP request
     * @return the login if the user is authenticated
     */
    @GetMapping("/authenticate")
    fun isAuthenticated(request: HttpServletRequest): String {
        log.debug("REST request to check if the current user is authenticated")
        return request.remoteUser
    }

    /**
     * POST  /account : update the current user information.
     * @param userDTO the current user information
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws RuntimeException          500 (Internal Server Error) if the user login wasn't found
     */
    @PostMapping("/account")
    fun saveAccount(@Valid @RequestBody userDTO: UserDTO) {
        userService.saveAccount(userDTO)
    }

    /**
     * POST  /account/change-password : changes the current user's password.
     * @param passwordChangeDto current and new password
     * @throws InvalidPasswordException 400 (Bad Request) if the new password is incorrect
     */
    @PostMapping(path = ["/account/change-password"])
    fun changePassword(@RequestBody passwordChangeDto: PasswordChangeDTO) {
        checkPasswordLength(passwordChangeDto.newPassword)
        userService.changePassword(passwordChangeDto.currentPassword, passwordChangeDto.newPassword)
    }

    /**
     * POST   /account/reset-password/init : Send an email to reset the password of the user.
     * @param mail the mail of the user
     * @throws EmailNotFoundException 400 (Bad Request) if the email address is not registered
     */
    @PostMapping(path = ["/account/reset-password/init"])
    fun requestPasswordReset(@RequestBody mail: String) {
        val user = userService.requestPasswordReset(mail)
        mailService.sendPasswordResetMail(user)
    }

    /**
     * POST   /account/reset-password/finish : Finish to reset the password of the user.
     * @param keyAndPassword the generated key and the new password
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     * @throws RuntimeException         500 (Internal Server Error) if the password could not be reset
     */
    @PostMapping(path = ["/account/reset-password/finish"])
    fun finishPasswordReset(@RequestBody keyAndPassword: KeyAndPasswordVM) {
        checkPasswordLength(keyAndPassword.newPassword)
        userService.completePasswordReset(keyAndPassword.newPassword, keyAndPassword.key)
    }

    companion object {

        private val log = LoggerFactory.getLogger(AccountResource::class.java)

        private fun checkPasswordLength(password: String) {
            if (StringUtils.isEmpty(password) ||
                    password.length < UserDTO.PASSWORD_MIN_LENGTH ||
                    password.length > UserDTO.PASSWORD_MAX_LENGTH) {

                throw InvalidPasswordException()
            }
        }
    }
}
