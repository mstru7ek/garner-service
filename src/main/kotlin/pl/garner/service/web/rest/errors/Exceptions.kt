package pl.garner.service.web.rest.errors

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status
import pl.garner.service.web.rest.errors.ErrorConstants.PROBLEM_BASE_URL
import java.net.URI
import java.util.*

open class BadRequestAlertException(
        type: URI,
        defaultMessage: String,
        val entityName: String,
        val errorKey: String
) : AbstractThrowableProblem(
        type,
        defaultMessage,
        Status.BAD_REQUEST,
        null,
        null,
        null,
        getAlertParameters(entityName, errorKey)
) {

    override fun getCause(): Exceptional {
        return this
    }

    companion object {
        private fun getAlertParameters(entityName: String, errorKey: String): Map<String, Any> {
            val parameters = HashMap<String, Any>()
            parameters["message"] = "error.$errorKey"
            parameters["params"] = entityName
            return parameters
        }
    }
}

class EmailAlreadyUsedException : BadRequestAlertException(TYPE, EXCEPTION_MESSAGE, ENTITY_NAME, ERROR_KEY) {
    companion object {
        private val TYPE = URI.create("$PROBLEM_BASE_URL/email-already-used")
        private const val EXCEPTION_MESSAGE = "Email is already in use!"
        private const val ENTITY_NAME = "userManagement"
        private const val ERROR_KEY = "emailexists"
    }
}

class EmailNotFoundException : BadRequestAlertException(TYPE, EXCEPTION_MESSAGE, ENTITY_NAME, ERROR_KEY) {
    companion object {
        private val TYPE = URI.create("$PROBLEM_BASE_URL/email-not-found")
        private const val EXCEPTION_MESSAGE = "Email address not registered"
        private const val ENTITY_NAME = "userManagement"
        private const val ERROR_KEY = "email"
    }
}

class InternalServerErrorException(message: String) :
        BadRequestAlertException(TYPE, message, ENTITY_NAME, ERROR_KEY) {

    companion object {
        private val TYPE = URI.create("$PROBLEM_BASE_URL/problem-with-message")
        private const val ENTITY_NAME = ""
        private const val ERROR_KEY = "internal"
    }
}

class InvalidPasswordException : BadRequestAlertException(TYPE, EXCEPTION_MESSAGE, ENTITY_NAME, ERROR_KEY) {
    companion object {
        private val TYPE = URI.create("$PROBLEM_BASE_URL/invalid-password")
        private const val EXCEPTION_MESSAGE = "Incorrect password"
        private const val ENTITY_NAME = "userManagement"
        private const val ERROR_KEY = "invalid_password"
    }
}

class LoginAlreadyUsedException : BadRequestAlertException(TYPE, EXCEPTION_MESSAGE, ENTITY_NAME, ERROR_KEY) {
    companion object {
        private val TYPE = URI.create("$PROBLEM_BASE_URL/login-already-used")
        private const val EXCEPTION_MESSAGE = "Login name already used!"
        private const val ENTITY_NAME = "userManagement"
        private const val ERROR_KEY = "user_exists"
    }
}

object ErrorConstants {

    const val ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure"
    const val ERR_VALIDATION = "error.validation"
    const val PROBLEM_BASE_URL = "https://www.jhipster.tech/problem"
    val CONSTRAINT_VIOLATION_TYPE = URI.create("$PROBLEM_BASE_URL/constraint-violation")
    val ENTITY_NOT_FOUND_TYPE = URI.create("$PROBLEM_BASE_URL/entity-not-found")
}