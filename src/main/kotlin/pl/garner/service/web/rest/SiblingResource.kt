package pl.garner.service.web.rest

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import org.zalando.problem.ThrowableProblem
import pl.garner.service.AddSiblingDto
import pl.garner.service.service.AccountContext
import pl.garner.service.service.SiblingService
import javax.validation.Valid

@RestController
@RequestMapping("/api/namespace/{nsId}/word/{wId}")
class SiblingResource(
        private val ac: AccountContext,
        private val siblingService: SiblingService
) {

    @GetMapping("/sibling")
    fun getAllSiblings(@PathVariable("wId") wordId: Long): ResponseEntity<*> {
        val siblings = siblingService.getAllSiblings(wordId)
        return ResponseEntity.ok(siblings)
    }

    @PostMapping("/sibling")
    fun createSiblings(@PathVariable("wId") wordId: Long,
                       @Valid @RequestBody addSiblingDto: AddSiblingDto): ResponseEntity<*> {
        siblingService.joinWords(wordId, addSiblingDto.wordId)
        val uri = UriComponentsBuilder.fromPath("/api/namespace/${ac.namespaceId}/word/${wordId}/sibling")
                .build().toUri()
        return ResponseEntity.created(uri).build<Any>()
    }

    @DeleteMapping("/sibling/{swId}")
    fun deleteSibling(@PathVariable("wId") wordId: Long,
                      @PathVariable("swId") siblingWordId: Long): ResponseEntity<*> {
        siblingService.deleteSiblingJoin(wordId, siblingWordId)
        return ResponseEntity.ok().build<Any>()
    }

    @GetMapping("/sibling/suggest")
    fun siblingSuggestions(@PathVariable("wId") wordId: Long,
                           @RequestParam("searchText") searchText: String): ResponseEntity<*> {
        val siblingSuggestions = siblingService.findSiblingSuggestions(wordId, searchText)
        return ResponseEntity.ok(siblingSuggestions)
    }
}
