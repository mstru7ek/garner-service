package pl.garner.service.web.rest.util

import kotlin.annotation.AnnotationTarget.*

/**
 * Annotation to mark the request method that could cause side effects in Http Session context.
 *
 */
@MustBeDocumented
@Target(FUNCTION, PROPERTY_GETTER, PROPERTY_SETTER, FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiSharedSession
