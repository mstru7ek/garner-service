package pl.garner.service.web.rest

import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.garner.service.service.TopicService
import pl.garner.service.web.rest.util.ResponseUtils

@RestController
@RequestMapping("/api/namespace/{nsId}/topic")
class TopicResource(private val topicService: TopicService) {

    @GetMapping("")
    fun findAllTopics(@PageableDefault(size = 50) pageable: Pageable): ResponseEntity<*> {
        val page = topicService.getAllTopics(pageable)
        return ResponseEntity.ok(page)
    }

    @GetMapping("/{tId}/word")
    fun findAllTopicWords(@PathVariable("tId") topicId: Long): ResponseEntity<*> {
        val words = topicService.getAllWords(topicId) ?: throw ResponseUtils.notFoundTopicEntity()
        return ResponseEntity.ok(words)
    }
}
