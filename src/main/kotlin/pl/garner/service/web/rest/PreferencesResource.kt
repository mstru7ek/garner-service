package pl.garner.service.web.rest

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.garner.service.UpdatePreferencesDto
import pl.garner.service.repository.NamespaceRepository
import pl.garner.service.service.AccountContext
import pl.garner.service.service.AccountService
import pl.garner.service.web.rest.util.ResponseUtils.notFoundNamespaceEntity
import javax.validation.Valid

@RestController
@RequestMapping("/api/preference")
class PreferencesResource(private val accountService: AccountService) {

    @GetMapping("")
    fun userPreferences(): ResponseEntity<*> = ResponseEntity.ok(accountService.userPreferences())

    @PutMapping("")
    fun updatePreferences(@Valid @RequestBody preference: UpdatePreferencesDto): ResponseEntity<*> {
        accountService.updateDefaultNamespace(preference.namespace.id) ?: throw notFoundNamespaceEntity()
        return ResponseEntity<Any>(HttpStatus.OK)
    }
}
