package pl.garner.service.web.rest.errors

import org.springframework.dao.ConcurrencyFailureException
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.NativeWebRequest
import org.zalando.problem.DefaultProblem
import org.zalando.problem.Problem
import org.zalando.problem.ProblemBuilder
import org.zalando.problem.Status
import org.zalando.problem.spring.web.advice.ProblemHandling
import org.zalando.problem.violations.ConstraintViolationProblem
import pl.garner.service.web.rest.util.HeaderUtil
import pl.garner.service.dropbox.DbxAuthorizationException
import pl.garner.service.dropbox.DbxServiceException
import java.net.URI
import java.util.*
import javax.servlet.http.HttpServletRequest

/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 * The error response follows RFC7807 - Problem Details for HTTP APIs (https://tools.ietf.org/html/rfc7807)
 */
@ControllerAdvice
class ExceptionTranslator : ProblemHandling {

    /**
     * Post-process the Problem payload to add the message key for the front-end if needed
     */

    override fun process(entity: ResponseEntity<Problem>, request: NativeWebRequest): ResponseEntity<Problem> {
        //  TODO unification : dbx & authorization
        return when (val problem = entity.body!!) {
            is ConstraintViolationProblem -> buildViolationProblem(problem, entity, request)
            is DefaultProblem -> buildDefaultProblem(problem, entity, request)
            else -> entity
        }
    }

    private fun buildDefaultProblem(problem: DefaultProblem,
                                    entity: ResponseEntity<Problem>,
                                    request: NativeWebRequest): ResponseEntity<Problem> {

        val builder = problemBuilder(problem, request)
        builder.withCause((problem).cause)
                .withDetail(problem.detail)
                .withInstance(problem.instance)

        problem.parameters.forEach { (key, value) -> builder.with(key, value) }

        if (!problem.parameters.containsKey("message") && problem.status != null) {
            builder.with("message", "error.http." + problem.status!!.statusCode)
        }
        return ResponseEntity(builder.build(), entity.headers, entity.statusCode)
    }

    private fun buildViolationProblem(problem: ConstraintViolationProblem,
                                      entity: ResponseEntity<Problem>,
                                      requestURI: NativeWebRequest): ResponseEntity<Problem> {

        val builder = problemBuilder(problem, requestURI)
        builder.with("violations", problem.violations)
                .with("message", ERR_VALIDATION)

        return ResponseEntity(builder.build(), entity.headers, entity.statusCode)
    }

    private fun problemBuilder(problem: Problem, request: NativeWebRequest): ProblemBuilder {
        val requestURI = request.getNativeRequest(HttpServletRequest::class.java)!!.requestURI
        return Problem.builder()
                .withType(problem.type)
                .withStatus(problem.status)
                .withTitle(problem.title)
                .with("path", requestURI)
    }

    override fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException, request: NativeWebRequest): ResponseEntity<Problem> {
        val result = ex.bindingResult
        val fieldErrors = result.fieldErrors.map { f -> FieldErrorVM(f.objectName, f.field, f.code ?: EMPTY_CODE) }
        val problem = Problem.builder()
                .withType(ErrorConstants.CONSTRAINT_VIOLATION_TYPE)
                .withTitle(METHOD_ARG_NOT_VALID)
                .withStatus(defaultConstraintViolationStatus())
                .with(MESSAGE_KEY, ErrorConstants.ERR_VALIDATION)
                .with(FIELD_ERRORS, fieldErrors)
                .build()
        return create(ex, problem, request)
    }

    @ExceptionHandler
    fun handleNoSuchElementException(ex: NoSuchElementException, request: NativeWebRequest): ResponseEntity<Problem> {
        val problem = Problem.builder()
                .withStatus(Status.NOT_FOUND)
                .with("message", ENTITY_NOT_FOUND_TYPE)
                .build()
        return create(ex, problem, request)
    }

    @ExceptionHandler
    fun handleBadRequestAlertException(ex: BadRequestAlertException,
                                       request: NativeWebRequest): ResponseEntity<Problem> {
        val httpHeaders = HeaderUtil.createFailureAlert(ex.entityName, ex.errorKey, ex.message!!)
        return create(ex, request, httpHeaders)
    }

    @ExceptionHandler
    fun handleConcurrencyFailure(ex: ConcurrencyFailureException, request: NativeWebRequest): ResponseEntity<Problem> {
        val problem = Problem.builder()
                .withStatus(Status.CONFLICT)
                .with(MESSAGE_KEY, ErrorConstants.ERR_CONCURRENCY_FAILURE)
                .build()
        return create(ex, problem, request)
    }

    @ExceptionHandler
    fun handleDbxAuthorizationExc(ex: DbxAuthorizationException, request: NativeWebRequest): ResponseEntity<Problem> {
        val problem = Problem.builder()
                .withType(DROPBOX_AUTHORIZATION)
                .withStatus(Status.INTERNAL_SERVER_ERROR)
                .withTitle("Dropbox authorization error")
                .withDetail(ex.message)
                .build()
        return create(ex, problem, request)
    }

    @ExceptionHandler
    fun handleDbxServiceExc(ex: DbxServiceException, request: NativeWebRequest): ResponseEntity<Problem> {
        val problem = Problem.builder()
                .withType(DROPBOX_SERVICE)
                .withStatus(Status.INTERNAL_SERVER_ERROR)
                .withTitle("Dropbox authorization error")
                .withDetail(ex.message)
                .build()
        return create(ex, problem, request)
    }

    companion object {
        private const val ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure"
        private const val EMPTY_CODE = ""
        private const val MESSAGE_KEY = "message"
        private const val METHOD_ARG_NOT_VALID = "Method argument not valid"
        private const val FIELD_ERRORS = "fieldErrors"
        private const val ERR_VALIDATION = "error.validation"
        private const val PROBLEM_BASE_URL = "https://www.jhipster.tech/problem"
        internal val CONSTRAINT_VIOLATION_TYPE = URI.create("$PROBLEM_BASE_URL/constraint-violation")
        internal val ENTITY_NOT_FOUND_TYPE = URI.create("$PROBLEM_BASE_URL/entity-not-found")

        private const val GRN_PROBLEM_BASE_URL = "https://garner-dbx.service/problem"
        private val DROPBOX_AUTHORIZATION: URI = URI.create("$GRN_PROBLEM_BASE_URL/dropbox-authorization")
        private val DROPBOX_SERVICE = URI.create("$GRN_PROBLEM_BASE_URL/dropbox-service")!!
    }

}


