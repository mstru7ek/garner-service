package pl.garner.service.web.rest

import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.garner.service.AddWordDto
import pl.garner.service.CoordinatesDto
import pl.garner.service.UpdateWordDto
import pl.garner.service.service.AccountContext
import pl.garner.service.service.WordService
import pl.garner.service.web.rest.rpc.ServiceDropboxRpc
import java.net.URI
import javax.validation.Valid

@RestController
@RequestMapping("/api/namespace/{nsId}/word")
class WordResource(
    private val ac: AccountContext,
    private val wordService: WordService,
    private val serviceDropboxRpc: ServiceDropboxRpc
) {

    @GetMapping
    fun listAllWords(
        @RequestParam(required = false) searchText: String?,
        @PageableDefault(size = 50) pageable: Pageable
    ): ResponseEntity<*> {
        val page = wordService.listWordsByNamespace(searchText, pageable)
        return ResponseEntity.ok(page)
    }

    @PostMapping
    fun createNewWord(@Valid @RequestBody addWordDto: AddWordDto): ResponseEntity<*> {
        val wordDto = wordService.createWord(addWordDto)
        val wordEntityLocation = URI.create("/api/namespace/${ac.namespaceId}/word/${wordDto.id}")
        return ResponseEntity.created(wordEntityLocation).body(wordDto)
    }

    @GetMapping("/{wId}")
    fun getWordById(@PathVariable("wId") wordId: Long): ResponseEntity<*> {
        return wordService.findOneById(wordId)
            .map { wordDto -> ResponseEntity(wordDto, HttpStatus.OK) }
            .orElseGet { ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR) }
    }

    @PutMapping("/{wId}")
    fun updateWord(
        @PathVariable("wId") wordId: Long,
        @Valid @RequestBody updateWordDto: UpdateWordDto
    ): ResponseEntity<*> {
        wordService.updateWord(wordId, updateWordDto)
        return ResponseEntity.ok().build<Any>()
    }

    @DeleteMapping("/{wId}")
    fun deleteWord(@PathVariable("wId") wordId: Long): ResponseEntity<*> {
        wordService.deactivateWord(wordId)
        // this method is not mandatory to succeed
        serviceDropboxRpc.deleteWordImages(ac.namespaceId, wordId)
        return ResponseEntity.ok().build<Any>()
    }

    @PutMapping("/{wId}/coordinate")
    fun updateCoordinates(
        @PathVariable("wId") wordId: Long,
        @Valid @RequestBody coordinatesDto: CoordinatesDto
    ): ResponseEntity<*> {
        val coordinate = wordService.updateCoordinates(wordId, coordinatesDto)
        return ResponseEntity.ok(coordinate)
    }
}
