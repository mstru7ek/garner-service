package pl.garner.service.web.rest.rpc

import org.springframework.stereotype.Service
import pl.garner.service.config.ApplicationProperties

@Service
class ServiceDropboxRpc(private val applicationProperties: ApplicationProperties)  {

    fun deleteWordImages(namespaceId: Long, wordId: Long) {
        throw IllegalStateException("Removed, should invoke DropboxService internaly")
    }
}
