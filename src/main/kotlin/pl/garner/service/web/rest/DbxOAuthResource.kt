package pl.garner.service.web.rest

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import pl.garner.service.DbxAccessTokenDto
import pl.garner.service.DbxAuthenticationDto
import pl.garner.service.DbxAuthorizeUri
import pl.garner.service.dropbox.DbxRegistryService
import pl.garner.service.web.rest.util.ApiSharedSession
import javax.servlet.http.HttpServletRequest

@RestController
class DbxOAuthResource(private val dbxRegistryService: DbxRegistryService) {

    @GetMapping("/api/dbx/accessToken/valid")
    fun isValidAccessToken(): ResponseEntity<DbxAccessTokenDto> {
        return ResponseEntity.ok()
                .body(DbxAccessTokenDto(dbxRegistryService.isValidAccessToken))
    }

    @ApiSharedSession
    @GetMapping("/api/dbx/connect")
    fun connectDropbox(servletRequest: HttpServletRequest): DbxAuthorizeUri {
        return dbxRegistryService.retrieveAuthorizeUrl(servletRequest)
    }

    @DeleteMapping("/api/dbx/disconnect")
    fun disconnectDropbox(servletRequest: HttpServletRequest): ResponseEntity<Void> {
        dbxRegistryService.revokeAuthorizationToken()
        return ResponseEntity.noContent().build()
    }

    @ApiSharedSession
    @GetMapping("/api/dbx/access-granted")
    fun dropboxAccessGranted(servletRequest: HttpServletRequest): DbxAuthenticationDto {
        return dbxRegistryService.confirmAccessGranted(servletRequest.parameterMap)
    }
}
