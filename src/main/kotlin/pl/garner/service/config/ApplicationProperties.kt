package pl.garner.service.config

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Properties specific to garner.
 *
 *
 * Properties are configured in the application.yml file.
 * See [io.github.jhipster.config.JHipsterProperties] for a good example.
 */

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
data class ApplicationProperties(var imageFixedWidthPx: Int = 300, var dropbox: Dropbox = Dropbox())

data class Dropbox(
        var key: String = "",
        var secret: String = "",
        var redirect: Redirect = Redirect()
)

data class Redirect(var uri: String = "")
