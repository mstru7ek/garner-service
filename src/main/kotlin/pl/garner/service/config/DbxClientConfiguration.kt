package pl.garner.service.config

import com.dropbox.core.DbxAppInfo
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DbxClientConfiguration(val appProperties: ApplicationProperties) {

    @Bean
    fun dbxAppInfo(): DbxAppInfo {
        return DbxAppInfo(
                appProperties.dropbox.key,
                appProperties.dropbox.secret)
    }
}
