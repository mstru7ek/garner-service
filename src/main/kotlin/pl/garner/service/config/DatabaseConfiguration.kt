package pl.garner.service.config

import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories("pl.garner.service.repository")
@EnableTransactionManagement
class DatabaseConfiguration