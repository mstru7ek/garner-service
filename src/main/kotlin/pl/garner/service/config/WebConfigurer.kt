package pl.garner.service.config

import io.github.jhipster.config.JHipsterProperties
import io.undertow.UndertowOptions
import org.slf4j.LoggerFactory
import org.springframework.boot.web.embedded.undertow.UndertowBuilderCustomizer
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory
import org.springframework.boot.web.server.WebServerFactory
import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import pl.garner.service.security.DbxClientV2Filter
import pl.garner.service.dropbox.DbxAccountService
import java.util.*
import javax.servlet.DispatcherType
import javax.servlet.ServletContext


/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
@Configuration
class WebConfigurer(
        private val env: Environment,
        private val dbxAccountService: DbxAccountService,
        private val jHipsterProperties: JHipsterProperties
) : ServletContextInitializer, WebServerFactoryCustomizer<WebServerFactory> {

    override fun onStartup(servletContext: ServletContext) {

        registerDbxClientV2Filter(servletContext)

        log.info("Web application configuration, using profiles: {}", Arrays.toString(env.activeProfiles))
        log.info("Web application fully configured")
    }

    /**
     * Customize the Servlet engine: Mime types, the document root, the cache.
     */
    override fun customize(server: WebServerFactory) {
        /*
         * Enable HTTP/2 for Undertow - https://twitter.com/ankinson/status/829256167700492288
         * HTTP/2 requires HTTPS, so HTTP requests will fallback to HTTP/1.1.
         * See the JHipsterProperties class and your application-*.yml configuration files
         * for more information.
         */
        val isHTTP2 = jHipsterProperties.http.getVersion() == JHipsterProperties.Http.Version.V_2_0
        if (isHTTP2 && server is UndertowServletWebServerFactory) {
            server.addBuilderCustomizers(UndertowBuilderCustomizer { builder ->
                builder.setServerOption(UndertowOptions.ENABLE_HTTP2, true)
            })
        }
    }

    @Bean
    fun corsFilter(): CorsFilter {
        val source = UrlBasedCorsConfigurationSource()
        val config = jHipsterProperties.cors
        if (config.allowedOrigins != null && !config.allowedOrigins!!.isEmpty()) {
            log.debug("Registering CORS filter")
            source.registerCorsConfiguration("/api/**", config)
        }
        return CorsFilter(source)
    }

    private fun registerDbxClientV2Filter(servletContext: ServletContext) {
        val dispatchers = EnumSet.allOf(DispatcherType::class.java)
        val registration = servletContext.addFilter("dbxClientV2Filter", DbxClientV2Filter(dbxAccountService))

        registration.addMappingForUrlPatterns(dispatchers, true, "/api/dbx/storage/upload")
        registration.addMappingForUrlPatterns(dispatchers, true, "/api/dbx/storage/upload/uri")
        registration.addMappingForUrlPatterns(dispatchers, true, "/api/dbx/storage/reload")
        registration.addMappingForUrlPatterns(dispatchers, true, "/api/dbx/disconnect")
        registration.addMappingForUrlPatterns(dispatchers, true, "/api/dbx/accessToken/valid")
        registration.setAsyncSupported(true)
    }

    companion object {
        private val log = LoggerFactory.getLogger(WebConfigurer::class.java)
    }
}
