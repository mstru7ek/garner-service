package pl.garner.service.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import pl.garner.service.service.AccountContext

import java.util.Optional

@Repository
interface NamespaceRepository : JpaRepository<Namespace, Long> {

    fun findAllByAccountInfoLogin(login: String): List<Namespace>

    fun findOneByNameAndAccountInfoLogin(name: String, login: String): Optional<Namespace>

    @Query("select n from Namespace n where n.id=:#{#nsId} and n.accountInfo.login=:#{#ac.login}")
    fun findOneById(@Param("nsId") id: Long, @Param("ac") ac: AccountContext): Namespace?

    @Query("select n from Namespace n where n.id=:#{#ac.namespaceId} and n.accountInfo.login=:#{#ac.login}")
    fun findByCurrent(@Param("ac") ac: AccountContext): Optional<Namespace>

}
