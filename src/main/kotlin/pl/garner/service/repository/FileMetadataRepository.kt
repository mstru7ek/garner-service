package pl.garner.service.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface FileMetadataRepository : JpaRepository<FileMetadata, String> {

    @Query("select m from FileMetadata m where m.login = ?1 and m.namespaceId = ?2 and m.wordId = ?3")
    fun findAllByWordId(login: String, namespaceId: Long?, wordId: Long?): List<FileMetadata>

    @Modifying
    @Query("delete from FileMetadata where login = ?1 and namespaceId = ?2 and wordId = ?3")
    fun deleteAllFiles(login: String, namespaceId: Long?, wordId: Long?)

    @Query("select m from FileMetadata m where m.login = ?1")
    fun findAllByLogin(login: String): List<FileMetadata>

    @Query("select m from FileMetadata m where m.login = ?1 and m.namespaceId = ?2")
    fun findAllInNamespace(login: String, namespaceId: Long?, pageable: Pageable): Page<FileMetadata>

    @Modifying
    @Query("delete from FileMetadata where login = ?1 and namespaceId = ?2 and url = ?3")
    fun deleteByUrl(login: String, namespaceId: Long?, url: String)
}
