package pl.garner.service.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import pl.garner.service.service.AccountContext

@Repository
interface AccountInfoRepository : JpaRepository<AccountInfo, Long> {

    @Query("select a from AccountInfo a left join fetch a.defaultNamespace where a.login=:#{#accountContext.login}")
    fun findCurrentBy(@Param("accountContext") accountContext: AccountContext): AccountInfo?
}
