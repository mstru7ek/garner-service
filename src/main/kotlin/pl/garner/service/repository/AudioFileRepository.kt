package pl.garner.service.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AudioFileRepository : JpaRepository<AudioFile, Long> {

    fun findAllByWord(pageable: Pageable, word: Word): Page<AudioFile>

    fun findAllByWordId(pageable: Pageable, wordId: Long): Page<AudioFile>

    fun deleteAllByWord(word: Word)
}
