package pl.garner.service.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface LinkRepository : JpaRepository<Link, Long> {

    @Query("select l from Link l where (l.word.id=:wId and l.sibling.word.id=:swId) or (l.word.id=:swId and l.sibling.word.id=:wId)")
    fun findAllSiblingLinks(@Param("wId") wordId: Long?, @Param("swId") siblingWordId: Long?): List<Link>

}
