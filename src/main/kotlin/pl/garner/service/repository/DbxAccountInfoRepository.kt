package pl.garner.service.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DbxAccountInfoRepository : JpaRepository<DbxAccountInfo, Long> {

    fun findByLogin(login: String): Optional<DbxAccountInfo>

    fun removeByLogin(login: String): Long
}
