package pl.garner.service.repository

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.springframework.data.annotation.CreatedDate
import java.io.Serializable
import java.time.Instant
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY
import javax.validation.constraints.NotNull
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

@Entity
data class Authority(
        @Id val name: String
) : Serializable {

    companion object {
        const val serialVersionUID = 1L

        fun of(name: String): Authority {
            return Authority(name = name)
        }
    }
}

@Entity
data class User(
        @Id @GeneratedValue(strategy = IDENTITY) var id: Long?,
        @Column(unique = true) val login: String,
        @Column(name = "password_hash") var password: String,
        @Column(name = "first_name") var firstName: String,
        @Column(name = "last_name") var lastName: String,
        @Column(unique = true) var email: String,

        @NotNull @Column(nullable = false) var activated: Boolean,
        @Column(name = "lang_key") var langKey: String = "pl",
        @Column(name = "image_url") var imageUrl: String?,

        @Column(name = "activation_key") var activationKey: String,

        @Column(name = "reset_key") var resetKey: String,
        @Column(name = "reset_date") var resetDate: Instant = Instant.EPOCH,

        @CreatedDate @Column(name = "created_date") val createdDate: Instant = Instant.now(),

        @ManyToMany(targetEntity = Authority::class)
        @JoinTable(
                name = "user_authority",
                joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "authority_name", referencedColumnName = "name")]
        )
        var authorities: Set<Authority> = HashSet()

) : Serializable


@MappedSuperclass
open class BaseEntity(
        @Id @GeneratedValue(strategy = IDENTITY)
        var id: Long? = null,
        var active: Boolean = true,
        var createdDate: LocalDateTime = LocalDateTime.now()
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BaseEntity
        return id == other.id && active == other.active && createdDate == other.createdDate
    }

    override fun hashCode(): Int {
        return Objects.hash(id, active, createdDate)
    }
}

@Entity
class AccountInfo(
        @Column(length = 50, unique = true, nullable = false)
        var login: String,

        @OneToOne
        var defaultNamespace: Namespace? = null,

        @OneToMany(mappedBy = "accountInfo")
        var namespaces: MutableList<Namespace> = ArrayList()
) : BaseEntity() {
    override fun toString(): String {
        return "AccountInfo(id=$id, login='$login', defaultNamespace=${defaultNamespace?.id})"
    }
}

@Entity
class Namespace(
        @ManyToOne
        var accountInfo: AccountInfo,

        @Column(length = 100)
        var name: String,

        @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST], targetEntity = Word::class)
        @OrderColumn(name = "id")
        var words: MutableList<Word> = ArrayList(),

        @OneToMany(mappedBy = "namespace", fetch = FetchType.LAZY, targetEntity = Topic::class)
        var topics: MutableList<Topic> = ArrayList()

) : BaseEntity() {

    override fun toString(): String {
        return "Namespace(id=$id, accountInfo=${accountInfo.id}, name=$name)"
    }
}

@Entity
class Topic(
        @ManyToOne
        var namespace: Namespace? = null,

        @Fetch(FetchMode.SUBSELECT)
        @OneToMany
        var links: MutableList<Link> = ArrayList(),

        @Fetch(FetchMode.SUBSELECT)
        @OneToMany
        var words: MutableList<Word> = ArrayList()


) : BaseEntity() {
    override fun toString(): String {
        return "Topic(id=$id, namespace=$namespace, links=#${links.size}, words=#${words.size})"
    }
}

@Entity
class Word(
        var name: String,
        var translation: String,
        var sentence: String?,
        var transcription: String?,
        var latitude: Double?,
        var longitude: Double?,
        @ManyToOne(fetch = FetchType.LAZY)
        var topic: Topic? = null
) : BaseEntity() {
    override fun toString(): String {
        return "Word(id=$id, name=$name, translation=$translation, topic=${topic?.id})"
    }
}

@Entity
class Link(
        @OneToOne var sibling: Link?,
        @ManyToOne var topic: Topic?,
        @ManyToOne var word: Word
) : BaseEntity() {
    fun siblingWordId(): Long = sibling!!.word.id!!

    override fun toString(): String {
        return "Link(id=$id, sibling=${sibling?.word?.id}, topic=${topic?.id}, word=${word.id})"
    }
}

@Entity
class AudioFile(
        @ManyToOne
        var word: Word,
        var name: String,
        var url: String
) : BaseEntity() {
    override fun toString(): String {
        return "AudioFile(id=$id, word=${word.id}, name=$name, url=$url)"
    }
}


@Entity
@Table(indexes = [
    Index(
            name = "idx_login_namespace_id_word_id",
            columnList = "login, namespaceId, wordId",
            unique = false)
])
data class FileMetadata(
        @Id
        val url: String,
        val id: String,
        val name: String,
        val type: String,
        val login: String,
        val namespaceId: Long,
        val wordId: Long,
        val wordName: String,
        val createdDate: LocalDateTime
)

@Entity
data class DbxAccountInfo(
        @Id
        @GeneratedValue(strategy = IDENTITY)
        val id: Long? = null,

        @NotNull
        @Column(length = COL_LOGIN_MAX_LENGTH, unique = true, nullable = false)
        val login: String,

        val dbxCsrfToken: String,
        val dbxUserId: String,
        val dbxAccountId: String,
        val dbxAccessToken: String
) {
    companion object {
        const val COL_LOGIN_MAX_LENGTH = 50
        val EMPTY = DbxAccountInfo(null, "", "", "", "", "")
    }
}