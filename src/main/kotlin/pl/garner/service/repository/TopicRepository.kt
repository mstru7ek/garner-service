package pl.garner.service.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*
import java.util.stream.Stream

@Repository
interface TopicRepository : JpaRepository<Topic, Long> {

    @Query("SELECT t FROM Topic t , IN(t.words) w WHERE w is empty AND t.namespace.id=:#{#namespaceId}")
    fun findAllEmpty(namespaceId: Long?): Stream<Topic>

    fun findOneByIdAndNamespaceId(topicId: Long, namespaceId: Long): Optional<Topic>

    @Query(value = "SELECT t FROM Namespace n, IN(n.topics) t WHERE n.id=:#{#namespaceId} AND t.words IS NOT EMPTY")
    fun findAllNonEmpty(pageable: Pageable, namespaceId: Long): Page<Topic>
}
