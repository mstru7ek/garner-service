package pl.garner.service.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import pl.garner.service.service.AccountContext
import java.util.Optional
import java.util.stream.Stream

@Repository
interface WordRepository : JpaRepository<Word, Long> {

    @Query("select w from Word w left join fetch w.topic " + "where w in (select wl from Namespace n , IN(n.words) wl where n.id=:#{#AC.namespaceId} and w.name like :#{#name})")
    fun findOneByName(@Param("name") name: String, @Param("AC") accountContext: AccountContext): Optional<Word>

    @Query("select l.sibling.word from Word w left join w.topic.links l where l.word=w and w.id=:wId")
    fun findAllSiblingsByWordId(@Param("wId") wordId: Long?): List<Word>

    @Query(
        value = "select w from Word w left join fetch w.topic where w in (select wl from Namespace n , IN(n.words) wl where n.id=:#{#AC.namespaceId} and ( :#{#searchText} is null or wl.name like '%' || :#{#searchText} || '%')) order by w.id desc",
        countQuery = "select count(wl) from Namespace n , IN(n.words) wl where n.id=:#{#AC.namespaceId} and ( :#{#searchText} is null or wl.name like '%' || :#{#searchText} || '%')"
    )
    fun findAllJoinTopicByNamespaceId(@Param("searchText") searchText: String?, @Param("AC") accountContext: AccountContext, pageable: Pageable): Page<Word>

    @Query("select wl from Namespace n , IN(n.words) wl where n.id=:#{#AC.namespaceId} and wl.id=:#{#wId}")
    fun findById(@Param("wId") wordId: Long?, @Param("AC") accountContext: AccountContext): Optional<Word>

    override fun findById(id: Long): Optional<Word>

    @Query("select w from Namespace n , IN(n.words) w where n.id=:#{#AC.namespaceId} " +
            "and w<>:#{#word} and (:#{#topic} is null or w.topic<>:#{#topic} or w.topic is null) " +
            "and ( w.name like CONCAT('%',:#{#searchText},'%') or w.translation like CONCAT('%',:#{#searchText},'%'))")
    fun findAllExcludeTopic(@Param("word") parentWord: Word, @Param("topic") topic: Topic?, @Param("searchText") searchText: String, @Param("AC") accountContext: AccountContext): Stream<Word>

    @Query("select w from Word w where w.id =:#{#wordId}")
    fun findOneByWordId(wordId: Long): Word?
}


