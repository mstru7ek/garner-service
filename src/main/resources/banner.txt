${AnsiColor.BRIGHT_BLUE}:: ${AnsiColor.RED}    ___     _     ___   _  _   ___   ___
${AnsiColor.BRIGHT_BLUE}:: ${AnsiColor.RED}   / __|   /_\   | _ \ | \| | | __| | _ \
${AnsiColor.BRIGHT_BLUE}:: ${AnsiColor.RED}  | (_ |  / _ \  |   / | .` | | _|  |   /
${AnsiColor.BRIGHT_BLUE}:: ${AnsiColor.RED}   \___| /_/ \_\ |_|_\ |_|\_| |___| |_|_\
${AnsiColor.BRIGHT_BLUE}::
${AnsiColor.BRIGHT_BLUE}:: Garner API Service 8082:: Running Spring Boot ${spring-boot.version} ::
