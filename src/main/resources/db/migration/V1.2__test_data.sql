-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.33.97    Database: garnerBeckend
-- ------------------------------------------------------
-- Server version	5.7.21


INSERT INTO `authority`(`name`) VALUES ('ROLE_ADMIN');
INSERT INTO `authority`(`name`) VALUES ('ROLE_ANONYMOUS');
INSERT INTO `authority`(`name`) VALUES ('ROLE_USER');


SET FOREIGN_KEY_CHECKS=0;


INSERT INTO `account_info` VALUES (1,1,'2018-10-06 13:35:35','mstruzek',1);

INSERT INTO `link` VALUES (5,1,'2018-10-06 13:37:28',6,2,1),(6,1,'2018-10-06 13:37:28',5,2,2),(7,1,'2018-10-06 13:38:02',8,4,4),(8,1,'2018-10-06 13:38:02',7,4,3);

INSERT INTO `namespace` VALUES (1,1,'2018-10-06 13:35:42','boczta',1);

INSERT INTO `namespace_words` VALUES (1,1,0),(1,2,1),(1,3,2),(1,4,3);

INSERT INTO `topic` VALUES (1,1,'2018-10-06 13:36:30',1),(2,1,'2018-10-06 13:37:28',1),(3,1,'2018-10-06 13:38:02',1),(4,1,'2018-10-06 13:38:37',1);

INSERT INTO `topic_links` VALUES (2,5),(2,6),(4,7),(4,8);

INSERT INTO `topic_words` VALUES (2,1),(2,2),(4,3),(4,4);

INSERT INTO `word` VALUES (1,1,'2018-10-06 13:35:58',NULL,NULL,'qwe',NULL,NULL,'qwe',2),(2,1,'2018-10-06 13:36:09',NULL,NULL,'asd',NULL,NULL,'asd',2),(3,1,'2018-10-06 13:36:18',NULL,NULL,'zxc',NULL,NULL,'zxc',4),(4,1,'2018-10-06 13:37:56',NULL,NULL,'bnm',NULL,NULL,'bnm',4);


SET FOREIGN_KEY_CHECKS=1;