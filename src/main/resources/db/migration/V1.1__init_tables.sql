-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.33.97    Database: garnerBeckend
-- ------------------------------------------------------
-- Server version	5.7.21


DROP TABLE IF EXISTS `authority`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `user_authority`;

CREATE TABLE `authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
);

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activated` bit(1) NOT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `email` varchar(254) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `lang_key` varchar(6) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `reset_date` datetime DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_user_login` (`login`),
  UNIQUE KEY `UK_user_email` (`email`)
);

CREATE TABLE `user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `FK_user_authority_authority_name` (`authority_name`)
);


ALTER TABLE `user_authority` ADD CONSTRAINT `FK_user_authority_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `authority` (`name`);
ALTER TABLE `user_authority` ADD CONSTRAINT `FK_user_authority_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);




--
-- Table structure for table `account_info`
--

DROP TABLE IF EXISTS `account_info`;
CREATE TABLE `account_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `default_namespace_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sa1ulywv4dkypujfdb762s9pe` (`login`),
  KEY `FKr8jrr52su4o1tijk251y45h3k` (`default_namespace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `namespace`
--

DROP TABLE IF EXISTS `namespace`;
CREATE TABLE `namespace` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `account_info_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK741eerr658sw0b6d1sq9cletp` (`account_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- circular dependency
--

ALTER TABLE `account_info` ADD CONSTRAINT `FKr8jrr52su4o1tijk251y45h3k` FOREIGN KEY (`default_namespace_id`) REFERENCES `namespace` (`id`);
ALTER TABLE `namespace` ADD CONSTRAINT `FK741eerr658sw0b6d1sq9cletp` FOREIGN KEY (`account_info_id`) REFERENCES `account_info` (`id`);

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `namespace_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdrnw62tcip66n3sf86fkdo10q` (`namespace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `topic` ADD CONSTRAINT `FKdrnw62tcip66n3sf86fkdo10q` FOREIGN KEY (`namespace_id`) REFERENCES `namespace` (`id`);

--
-- Table structure for table `word`
--

DROP TABLE IF EXISTS `word`;
CREATE TABLE `word` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sentence` varchar(255) DEFAULT NULL,
  `transcription` varchar(255) DEFAULT NULL,
  `translation` varchar(255) DEFAULT NULL,
  `topic_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgmupqufao4t8fv638vhvmn7qs` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `word` ADD CONSTRAINT `FKgmupqufao4t8fv638vhvmn7qs` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`);


--
-- Table structure for table `audio_file`
--

DROP TABLE IF EXISTS `audio_file`;
CREATE TABLE `audio_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `word_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl8sf5syceela65pwxdujte8vk` (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `audio_file` ADD CONSTRAINT `FKl8sf5syceela65pwxdujte8vk` FOREIGN KEY (`word_id`) REFERENCES `word` (`id`);

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
CREATE TABLE `link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `sibling_id` bigint(20) DEFAULT NULL,
  `topic_id` bigint(20) DEFAULT NULL,
  `word_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1upmu6r0lefe7fv5wlqarner3` (`sibling_id`),
  KEY `FKs41m3cfyf0ogofsvbku9x8xjm` (`topic_id`),
  KEY `FK10e2y42wdtnipcnh5mfab57f1` (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `link` ADD CONSTRAINT `FK10e2y42wdtnipcnh5mfab57f1` FOREIGN KEY (`word_id`) REFERENCES `word` (`id`);
ALTER TABLE `link` ADD CONSTRAINT `FK1upmu6r0lefe7fv5wlqarner3` FOREIGN KEY (`sibling_id`) REFERENCES `link` (`id`);
ALTER TABLE `link` ADD CONSTRAINT `FKs41m3cfyf0ogofsvbku9x8xjm` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`);

--
-- Table structure for table `namespace_words`
--

DROP TABLE IF EXISTS `namespace_words`;
CREATE TABLE `namespace_words` (
  `namespace_id` bigint(20) NOT NULL,
  `words_id` bigint(20) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`namespace_id`,`id`),
  UNIQUE KEY `UK_sp8j4ue1wo5v3gbx18oj68a2` (`words_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `namespace_words` ADD CONSTRAINT `FK42cxn3omr6xia7wx03c2sc69i` FOREIGN KEY (`words_id`) REFERENCES `word` (`id`);
ALTER TABLE `namespace_words` ADD CONSTRAINT `FKnd115uj4ml2cqlvyvvvedqs43` FOREIGN KEY (`namespace_id`) REFERENCES `namespace` (`id`);

--
-- Table structure for table `topic_links`
--

DROP TABLE IF EXISTS `topic_links`;
CREATE TABLE `topic_links` (
  `topic_id` bigint(20) NOT NULL,
  `links_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_fvwtso59o7snjo6qd4fh0hqe7` (`links_id`),
  KEY `FKchfhywi042h9icftdrsbippa9` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `topic_links` ADD CONSTRAINT `FKchfhywi042h9icftdrsbippa9` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`);
ALTER TABLE `topic_links` ADD CONSTRAINT `FKs0kg1rpbsd4c2kq3jq3qho60u` FOREIGN KEY (`links_id`) REFERENCES `link` (`id`);

--
-- Table structure for table `topic_words`
--

DROP TABLE IF EXISTS `topic_words`;
CREATE TABLE `topic_words` (
  `topic_id` bigint(20) NOT NULL,
  `words_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_oqi8rgmbxkpcxysid8ch6v05f` (`words_id`),
  KEY `FK95xt8lqruj08wxg9btqbm9d5s` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `topic_words` ADD CONSTRAINT `FK5qk25b26pu5bbhcu3ks6h8oyl` FOREIGN KEY (`words_id`) REFERENCES `word` (`id`);
ALTER TABLE `topic_words` ADD CONSTRAINT `FK95xt8lqruj08wxg9btqbm9d5s` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`);

--
--
--