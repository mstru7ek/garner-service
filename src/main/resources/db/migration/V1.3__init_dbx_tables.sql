DROP TABLE IF EXISTS `dbx_account_info`;

CREATE TABLE `dbx_account_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dbx_access_token` varchar(255) DEFAULT NULL,
  `dbx_account_id` varchar(255) DEFAULT NULL,
  `dbx_csrf_token` varchar(255) DEFAULT NULL,
  `dbx_user_id` varchar(255) DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dbx_account_info_login` (`login`)
);


DROP TABLE IF EXISTS `image_file`;

CREATE TABLE `file_metadata` (
  `url` varchar(255) NOT NULL,
  `id` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `namespace_id` bigint(20) DEFAULT NULL,
  `word_id` bigint(20) DEFAULT NULL,
  `word_name` VARCHAR(255) NOT NULL,
  `created_date` DATETIME,
  PRIMARY KEY (`url`),
  KEY `UK_file_metadata_login_namespace_id_word_id` (`login`,`namespace_id`,`word_id`)
);

