For those blessed with MySQL >= 5.1.12, you can control this option globally at runtime:

Execute SET GLOBAL log_output = 'TABLE';
Execute SET GLOBAL general_log = 'ON';
Take a look at the table mysql.general_log
If you prefer to output to a file instead of a table:

SET GLOBAL log_output = "FILE"; the default.
SET GLOBAL general_log_file = "D:\workspace\garner-service\build\mysql-garner-db.log";
SET GLOBAL general_log = 'ON';

SELECT event_time, CONVERT(argument USING latin1) FROM mysql.general_log ORDER BY event_time DESC;



---

-- full drop gradlew :flywayClean
