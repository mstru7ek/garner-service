# garner
This application was generated using JHipster 5.3.4, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v5.3.4](https://www.jhipster.tech/documentation-archive/v5.3.4).

This is a "microservice" application intended to be part of a microservice architecture, please refer to the [Doing microservices with JHipster][] page of the documentation for more information.

This application is configured for Service Discovery and Configuration with . On launch, it will refuse to start if it is not able to connect to .

## Development

To start your application in the dev profile, simply run:

    ./gradlew


Operations in Windows Env
-------------------------

POWERSHELL:

lista taskow

    TAKSLIST


list process z otwartym portem

    NETSTAT -a -n -o | grep 8083

wymus zamkniecie processu

    TAKSKILL /PID n /F





For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].



## Building for production

To optimize the garner application for production, run:

    ./gradlew -Pprod clean bootWar

To ensure everything worked, run:

    java -jar build/libs/*.war


Refer to [Using JHipster in production][] for more details.

## Testing

To launch your application's tests, run:

    ./gradlew test

For more information, refer to the [Running tests page][].

### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

Then, run a Sonar analysis:

```
./gradlew -Pprod clean test sonarqube
```

For more information, refer to the [Code quality page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.

For example, to start a mysql database in a docker container, run:

    docker-compose -f src/main/docker/mysql.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/mysql.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./gradlew bootWar -Pprod buildDocker

Then run:

    docker-compose -f src/main/docker/app.yml up -d

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

## Continuous Integration (optional)

To configure CI for your project, run the ci-cd sub-generator (`jhipster ci-cd`), this will let you generate configuration files for a number of Continuous Integration systems. Consult the [Setting up Continuous Integration][] page for more information.

## Docker repository deploy

Build docker application image

    ./gradlew bootWar -Pprod buildDocker

Login in with credentials -> username: struzek,  pass: docker80

    docker login

Then push image artifact to public repository

    docker push struzek/garner-service:0.0.4-SNAPSHOT


[JHipster Homepage and latest documentation]: https://www.jhipster.tech
[JHipster 5.3.4 archive]: https://www.jhipster.tech/documentation-archive/v5.3.4
[Doing microservices with JHipster]: https://www.jhipster.tech/documentation-archive/v5.3.4/microservices-architecture/
[Using JHipster in development]: https://www.jhipster.tech/documentation-archive/v5.3.4/development/
[Using Docker and Docker-Compose]: https://www.jhipster.tech/documentation-archive/v5.3.4/docker-compose
[Using JHipster in production]: https://www.jhipster.tech/documentation-archive/v5.3.4/production/
[Running tests page]: https://www.jhipster.tech/documentation-archive/v5.3.4/running-tests/
[Code quality page]: https://www.jhipster.tech/documentation-archive/v5.3.4/code-quality/
[Setting up Continuous Integration]: https://www.jhipster.tech/documentation-archive/v5.3.4/setting-up-ci/




# SSEvents
# https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/servlet/mvc/method/annotation/SseEmitter.html
https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events
https://www.baeldung.com/spring-server-sent-events